var Register = (function () {

    var $phoneInput = $('#fos_user_registration_form_phone'),
        $nameInput = $('#fos_user_registration_form_name'),
        $emailInput = $('#fos_user_registration_form_email'),
        $passwordInput = $('#fos_user_registration_form_plainPassword'),
        phoneConfirmed = false;

    function init (sendSmsUrl, smsConfirmUrl) {
        $phoneInput.mask('375000000000', {placeholder: "375"});

        $('#register-form').on('submit', function (e) {
            validate(function () {
                if (!phoneConfirmed) {
                    e.preventDefault();
                }
            });
        });

        $nameInput.on('keyup', function () {
            validate();
        });

        $emailInput.on('keyup', function () {
            $(this).css('borderColor', '#eee');
            validate();
        });

        $passwordInput.on('keyup', function () {
            validate();
        });

        $phoneInput.on('keyup change', function () {
            validate();
        });

        $('#reg-btn').on('click', function () {
            var that = this;

            console.log('sdfsdf');

            validate(function () {
                $(that).attr('disabled', true);
                $(that).hide();

                $nameInput.attr('readonly', true);
                $phoneInput.attr('readonly', true);
                $emailInput.attr('readonly', true);
                $passwordInput.attr('readonly', true);

                $.post(Routing.generate('ajax_sms_send'), {phone: $phoneInput.val(), email: $emailInput.val()}, function (response) {


                    if (response.status == false) {
                        $emailInput.attr('readonly', false);
                        $emailInput.css('borderColor', 'red');
                        $(that).attr('disabled', false);
                        $(that).show();
                    } else {
                        $('#sms-form').show();
                    }


                })
            });
        });

        $('#sms-code').on('keyup', function () {
            var value = $('#sms-code').val();
            var that = $(this);
            $(that).css('borderColor', '#eee');

            if (value.length == 5) {
                $(that).attr('readonly', true);
                if (!phoneConfirmed) {
                    $('#confirm').button('loading');
                    $.post(Routing.generate('ajax_sms_confirm'), {code: value}, function (response) {
                        if (response.status == true) {
                            phoneConfirmed = true;
                            $('#confirm').attr('disabled', false);
                            $('#confirm').button('reset');
                        } else {

                            $(that).css('borderColor', 'red');
                            $(that).val('');
                            $(that).attr('readonly', false);


                            $('#confirm').attr('disabled', true);

                        }
                    });
                }



            }
        })
    }

    function validate (callback) {
        var error = false;

        if (!$nameInput.val()) {
            error = true;
        }

        if (!$emailInput.val()) {
            error = true;
        }

        if (!$passwordInput.val()) {
            error = true;
        }

        if ($phoneInput.val().length != 12) {
            error = true;
        }

        if (!error) {

            if (callback) {
                callback();
            }
        }


    }

    return {
        init: init
    }
}());