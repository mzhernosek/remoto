$(function () {

    $('#login-form').on('submit', function (e) {
        e.preventDefault();

        var $that = $(this);
        var action = $(this).attr('action');
        var data = $(this).serialize();
        var error = false;

        if (!$('#username').val()) {
            $('#username').addClass('error');
            error = true;
        }

        if (!$('#password').val()) {
            $('#password').addClass('error');
            error = true;
        }

        if (!error) {
            $.post(action, data, function (response) {
                if (response.success) {
                    window.location.href = response.url;
                } else {
                    $that.find('input[type=email]').addClass('error');
                    $that.find('input[type=password]').addClass('error');
                    $that.find('.error-message').show();
                }
            })
        }
    })

});