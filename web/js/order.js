var Order = (function () {

    var $markInput = $('#order-mark'),
        $modelInput = $('#order-model'),
        $yearInput = $('#order-year'),
        $textInput = $('#order-text'),
        $modalModelInput = $('#modal-order-model'),
        $modalYearInput = $('#modal-order-year'),
        $modalTextInput = $('#modal-order-text'),
        $modalMarkInput = $('#modal-order-mark');

    function initBase () {

        initFirst();

        $textInput.on('keyup', function () {
            if ($(this).val()) {

            }
        });

    }

    function initFirst () {
        $markInput.on('change', function () {
            var id = $(this).val();

            if (id) {
                var url = Routing.generate('ajax_get_models', {id: id});
                var $modelSelect = $modelInput;
                $modelSelect.attr('disabled', false);

                $.get(url, function (response) {
                    $modelSelect.empty();
                    $modelSelect
                        .append($("<option></option>")
                            .attr("value","0")
                            .text("Модель"));

                    $.each(response.data, function(key, value) {
                        $modelSelect
                            .append($("<option></option>")
                                .attr("value",value.id)
                                .text(value.title));
                    });

                    $modelSelect.selectpicker('refresh');
                });
            }
        });

        $modelInput.on('change', function () {
            var id = $(this).val();

            if (id) {
                $yearInput.attr('disabled', false);
            }

            $yearInput.selectpicker('refresh');
        });

        $yearInput.on('change', function () {
            var year = $(this).val();

            if (year) {
                $textInput.attr('disabled', false);
            }
        });

        $textInput.on('keyup', function () {
            if ($(this).val()) {
                $('#order-btn').attr('disabled', false);
            }
        });
    }

    function initFull (modelId) {
        initFirst();

        if ($('#order-year').val()) {
            $('#order-year').attr('disabled', false);
        }

        if ($textInput.val()) {
            $textInput.attr('disabled', false);
        }

        if (modelId) {
            var id = $markInput.val();

            if (id) {
                var url = Routing.generate('ajax_get_models', {id: id});
                var $modelSelect = $modelInput;
                $modelSelect.attr('disabled', false);

                $.get(url, function (response) {
                    $modelSelect.empty();
                    $modelSelect
                        .append($("<option></option>")
                            .attr("value","0")
                            .text("Модель"));

                    $.each(response.data, function(key, value) {
                        $modelSelect
                            .append($("<option></option>")
                                .attr("value",value.id)
                                .text(value.title));
                    });

                    $modelSelect.val(modelId);

                    $modelSelect.selectpicker('refresh');
                });
            }
        }

        $(document).on('click', '.remove', function () {
            console.log('removed');
        });

        $('#order-date').datepicker({
            autoclose: true,
            weekStart: 1,
            format: "yyyy-mm-dd",
            startDate: new Date(),
            language: 'ru'
        });

        $('#fileupload').fileupload({
            dataType: 'json',
            done: function (e, data) {

                console.log(data);

                $.each(data.result.files, function (index, file) {
                    $('#files')
                        .append('<div class="image"><img width="200px" class="img-responsive" src="/uploads/tmp/'+file+'"><span class="remove"><i class="fa fa-remove"></i></span></div>')

                    $('#files')
                        .append('<input type="hidden" value='+file+' name="images[]">');
                });
            }
        });
    }

    function initIndexForm () {

        function saveData () {
            $.post(Routing.generate('ajax_save_data'), {
                year: $modalYearInput.val(),
                text: $modalTextInput.val(),
                mark: $modalMarkInput.val(),
                model: $modalModelInput.val()
            }, function (){

            });
        }


        function loadModels (setId) {
            var that = $modalMarkInput;
            var id = $(that).val();

            if (id) {
                var url = Routing.generate('ajax_get_models', {id: id});
                var $modelSelect  = $modalModelInput;
                $modelSelect.attr('disabled', false);

                $.get(url, function (response) {
                    $modelSelect.empty();
                    $modelSelect
                        .append($("<option></option>")
                            .attr("value","0")
                            .text("Модель"));

                    $.each(response.data, function(key, value) {
                        $modelSelect
                            .append($("<option></option>")
                                .attr("value",value.id)
                                .text(value.title));
                    });

                    if (setId) {
                        $modalModelInput.val(setId);
                        saveData();
                    }

                    $modalModelInput.selectpicker('refresh');
                });
            }
        }


        $modalModelInput.on('change', saveData);
        $modalTextInput.on('keyup', saveData);
        $modalYearInput.on('change', saveData);

        $('#modal-order-mark').on('change', loadModels);
        $('#modal-order-mark').on('change', saveData);

        $('#index-form').on('submit', function (e) {
            e.preventDefault();

            $('#modal-order-mark').val($markInput.val());
            $('#modal-order-mark').selectpicker('refresh');
            loadModels($modelInput.val());

            $modalYearInput.val($yearInput.val());
            $modalYearInput.attr('disabled', false);
            $modalYearInput.selectpicker('refresh');

            $modalTextInput.val($textInput.val());
            $modalTextInput.attr('disabled', false);

            saveData();

            $('#order-modal').modal('show');
        })

    }

    return {
        initBase: initBase,
        initFull: initFull,
        initIndexForm: initIndexForm
    }
} ());