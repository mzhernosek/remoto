'use strict';
module.exports = function (grunt) {

    grunt.initConfig({

        less: {
            front: {
                options: {
                    compress: true
                },
                files: {
                    "static/dist/front/less.css": "static/front/less/main.less"
                }
            },
            profile: {
                options: {
                    compress: true
                },
                files: {
                    "static/dist/profile/less.css": "static/profile/less/main.less"
                }
            },
            admin: {
                options: {
                    compress: true
                },
                files: {
                    "static/dist/admin/less.css": "static/admin/less/main.less"
                }
            }
        },
        concat: {
            frontCss: {
                src: [
                    './static/common/css/bootstrap.css',
                    './static/dist/front/less.css'

                ],
                dest: './static/dist/front/front.css'
            },
            profileCss: {
                src: [
                    './static/common/css/bootstrap.css',
                    './static/dist/profile/less.css'
                ],
                dest: './static/dist/profile/profile.css'
            },
            adminCss: {
                src: [
                    './static/common/css/bootstrap.css',
                    './static/admin/css/xeditable.css',
                    './static/admin/css/angular-ui-notification.min.css',
                    './static/admin/css/textAngular.css',
                    './static/admin/css/ng-table.min.css',
                    './static/dist/admin/less.css'

                ],
                dest: './static/dist/admin/admin.css'
            },
            profileAngular: {
                src: [
                    './static/profile/js/app.js',
                    './static/profile/js/controller/*.js',
                    './static/profile/js/controller/**/*.js',
                    './static/profile/js/directive/*.js',
                    './static/profile/js/service/*.js',
                    './static/profile/js/service/**/*.js'
                ],
                dest: './static/dist/profile/js/angular.js'
            },
            adminAngular: {
                src: [
                    './static/admin/js/app.js',
                    './static/admin/js/controller/*.js',
                    './static/admin/js/controller/**/*.js',
                    './static/admin/js/directive/*.js',
                    './static/admin/js/service/*.js',
                    './static/admin/js/service/**/*.js'
                ],
                dest: './static/dist/admin/js/angular.js'
            },
            profileJs: {
                src: [
                    './static/profile/js/js/angular.js',
                    './static/profile/js/js/angular-ui-router.min.js',
                    './static/profile/js/js/angular-ui-notification.min.js',
                    './static/profile/js/js/ui-bootstrap-tpls-0.13.3.min.js',
                    './static/profile/js/js/angular-ui-tree.min.js',
                    './static/profile/js/js/mfb.min.js',
                    './static/profile/js/js/xeditable.min.js',
                    './static/profile/js/js/http-auth-interceptor.js',
                    './static/profile/js/js/angular-confirm.js',
                    './static/profile/js/js/raphael.js',
                    './static/profile/js/js/ng-file-upload.min.js',
                    './static/profile/js/js/spin.min.js',
                    './static/profile/js/js/ladda.min.js',
                    './static/profile/js/js/angular-ladda.js',
                    './static/profile/js/js/angular-resizable.min.js',
                    './static/profile/js/js/angular-screenfull.js',
                    './static/profile/js/js/object-table.js',
                    './static/profile/js/js/ng-infinite-scroll.min.js',


                    './static/dist/profile/js/angular.js',
                    './static/profile/js/titles.js',
                    './static/dist/profile/js/templates.js'
                ],
                dest: './web/js/profile.min.js'
            },
            adminJs: {
                src: [
                    './static/admin/js/js/angular.js',
                    './static/admin/js/js/angular-ui-router.min.js',
                    './static/admin/js/js/angular-ui-notification.min.js',
                    './static/admin/js/js/ui-bootstrap-tpls-0.13.3.min.js',
                    './static/admin/js/js/angular-ui-tree.min.js',
                    './static/admin/js/js/mfb.min.js',
                    './static/admin/js/js/xeditable.min.js',
                    './static/admin/js/js/http-auth-interceptor.js',
                    './static/admin/js/js/angular-confirm.js',
                    './static/admin/js/js/raphael.js',
                    './static/admin/js/js/ng-file-upload.min.js',
                    './static/admin/js/js/ng-table.min.js',
                    './static/admin/js/js/spin.min.js',
                    './static/admin/js/js/ladda.min.js',
                    './static/admin/js/js/angular-ladda.js',
                    './static/admin/js/js/angular-resizable.min.js',
                    './static/admin/js/js/angular-screenfull.js',
                    './static/admin/js/js/object-table.js',
                    './static/admin/js/js/ng-infinite-scroll.min.js',
                    './static/admin/js/js/tiny.js',
                    './static/admin/js/js/angular-summernote.min.js',


                    './static/dist/admin/js/angular.js',
                    './static/admin/js/titles.js',
                    './static/dist/admin/js/templates.js'
                ],
                dest: './web/js/admin.min.js'
            },
            css: {
                src: [
                    './front/app/css/bootstrap.min.css',
                    './front/app/css/angular-ui-notification.min.css',
                    './front/app/css/angular-ui-tree.min.css',
                    './front/app/css/mfb.min.css',
                    './front/app/css/xeditable.css',
                    './front/app/css/ladda-themeless.min.css',
                    './front/app/css/angular-resizable.min.css',
                    './front/dist/app/css/index.min.css',
                    './front/dist/app/css/admin-lte.min.css'
                ],
                dest: './front/dist/app/css/style.min.css'
            },
            angular: {
                src: [
                    './front/app/js/app.js',
                    './front/account/js/app.js',
                    './front/admin/js/app.js',
                    './front/common/js/app.js',
                    './front/anon/js/app.js',

                    './front/app/js/controller/*.js',
                    './front/app/js/directive/*.js',
                    './front/app/js/service/*.js',
                    './front/account/js/**/*.js',
                    './front/account/js/**/**/*.js',
                    './front/admin/js/controller/*.js',
                    './front/admin/js/directive/*.js',
                    './front/admin/js/service/*.js',
                    './front/admin/js/service/model/*.js',
                    './front/common/js/service/*.js',
                    './front/common/js/service/model/*.js',
                    './front/anon/js/controller/*.js',
                    './front/anon/js/directive/*.js',
                    './front/anon/js/service/*.js'
                ],
                dest: './front/dist/app/js/angular.js'
            },
            js: {
                src: [
                    './front/app/js/js/angular.js',
                    './front/app/js/js/angular-ui-router.min.js',
                    './front/app/js/js/angular-ui-notification.min.js',
                    './front/app/js/js/ui-bootstrap-tpls-0.13.3.min.js',
                    './front/app/js/js/angular-ui-tree.min.js',
                    './front/app/js/js/mfb.min.js',
                    './front/app/js/js/xeditable.min.js',
                    './front/app/js/js/http-auth-interceptor.js',
                    './front/app/js/js/angular-confirm.js',
                    './front/app/js/js/raphael.js',
                    './front/app/js/js/ng-file-upload.min.js',
                    './front/app/js/js/spin.min.js',
                    './front/app/js/js/ladda.min.js',
                    './front/app/js/js/angular-ladda.js',
                    './front/app/js/js/angular-resizable.min.js',
                    './front/app/js/js/angular-screenfull.js',
                    './front/app/js/js/object-table.js',
                    './front/app/js/js/ng-infinite-scroll.min.js',

                    './front/dist/app/js/angular.js',
                    './front/app/js/titles.js',
                    './front/dist/app/js/templates.js',
                    './front/dist/app/js/account_templates.js',
                    './front/dist/app/js/admin_templates.js',
                    './front/dist/app/js/anon_templates.js'
                ],
                dest: './web/js/app.min.js'
            }
        },
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            app: {
                files: {
                    './static/dist/profile/js/angular.js': ['./static/dist/profile/js/angular.js']
                }
            },
            admin: {
                files: {
                    './static/dist/admin/js/angular.js': ['./static/dist/admin/js/angular.js']
                }
            }
        },
        cssmin: {
            options: {
                keepSpecialComments: 0
            },
            front: {
                files: {
                    './web/css/front.min.css': [
                        './static/dist/front/front.css'
                    ]
                }
            },
            profile: {
                files: {
                    './web/css/profile.min.css': [
                        './static/dist/profile/profile.css'
                    ]
                }
            },
            admin: {
                files: {
                    './web/css/admin.min.css': [
                        './static/dist/admin/admin.css'
                    ]
                }
            }
        },
        uglify: {
            js: {
                src: ['./web/js/profile.min.js'],
                dest: './web/js/profile.min.js'
            },
            admin: {
                src: ['./web/js/admin.min.js'],
                dest: './web/js/admin.min.js'
            }
        },
        ngtemplates:  {
            app: {
                src:      ['./static/profile/js/templates/*.html', './static/profile/js/templates/**/*.html'],
                dest:     './static/dist/profile/js/templates.js',
                options:    {
                    url:    function(url) {
                        return '/app-js'+url.replace('./static/profile/js','');
                    },
                    htmlmin:  { collapseWhitespace: true, collapseBooleanAttributes: true }
                }
            },
            admin: {
                src:      ['./static/admin/js/templates/*.html', './static/admin/js/templates/**/*.html'],
                dest:     './static/dist/admin/js/templates.js',
                options:    {
                    url:    function(url) {
                        return '/admin-js'+url.replace('./static/admin/js','');
                    },
                    htmlmin:  { collapseWhitespace: true, collapseBooleanAttributes: true }
                }
            }
        },

        spriteGenerator: {
            sprite: {
                compositor: 'jimp',
                src: [
                    './static/profile/img/*.png'
                ],
                spritePath: './web/css/sprites.png',
                stylesheetPath: './web/css/sprites.css',
                stylesheet: 'css',
                stylesheetOptions: {
                    prefix: 'icon-'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-angular-templates');

    grunt.loadNpmTasks('node-sprite-generator');

    grunt.registerTask('buildFrontCss', ['less:front', 'concat:frontCss', 'cssmin:front']);
    grunt.registerTask('buildFrontJs', []);
    grunt.registerTask('buildProfileCss', ['less:profile', 'concat:profileCss', 'cssmin:profile']);
    grunt.registerTask('buildProfileJs', ['concat:profileAngular', 'ngAnnotate:app', 'ngtemplates:app', 'concat:profileJs', 'uglify:js']);
    grunt.registerTask('buildAdminCss', ['less:admin', 'concat:adminCss', 'cssmin:admin']);
    grunt.registerTask('buildAdminJs', [
        'concat:adminAngular',
        'ngAnnotate:admin',
        'ngtemplates:admin',
        'concat:adminJs',
        'uglify:admin']);
};