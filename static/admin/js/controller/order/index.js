(function() {
    'use strict';

    angular
        .module('admin')
        .controller('OrdersController', controller);

    function controller(
        $rootScope,
        Order,
        NgTableParams
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Заявки';

        vm.table = new NgTableParams({}, {
            getData: function(params) {
                return Order.all(params.url()).then(function(data) {
                    params.total(data.data.count);
                    return data.data.data;
                });
            }
        });

        return this;
    }

})();