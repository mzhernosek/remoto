(function() {
    'use strict';

    angular
        .module('admin')
        .controller('OrderPageController', controller);

    function controller(
        $rootScope,
        $scope,
        Order,
        $state,
        Notification,
        $stateParams
    ) {
        var vm = this;


        $rootScope.pageTitle = 'Страница заявки';

        vm.order = {};
        vm.answers = [];

        Order
            .find($stateParams.id)
            .then(function (response) {
                vm.order = response.data;
            });

        Order
            .getAnswers($stateParams.id)
            .then(function (response) {
                vm.answers = response.data;
            });

        vm.save = function () {
            Order
                .update(vm.order)
                .then(function(response){
                    Notification.success('Заявка успешно обновлена');
                });
        };

        return this;
    }

})();