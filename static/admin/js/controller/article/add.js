(function() {
    'use strict';

    angular
        .module('admin')
        .controller('ArticleAddController', controller);

    function controller(
        $rootScope,
        Article,
        $stateParams,
        $state,
        Notification
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Добавление статьи';

        vm.article = {};

        vm.save = function () {
            Article
                .add(vm.article)
                .then(function (response) {
                    Notification.success('Статья успешно добавлена');
                    $state.go('articles');
                })
        };


        return this;
    }

})();