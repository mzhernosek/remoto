(function() {
    'use strict';

    angular
        .module('admin')
        .controller('ArticlesController', controller);

    function controller(
        $rootScope,
        Article,
        NgTableParams,
        $confirm,
        Notification
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Статьи';

        vm.table = new NgTableParams({}, {
            getData: function(params) {
                return Article.all(params.url()).then(function(data) {
                    params.total(data.data.count);
                    return data.data.data;
                });
            }
        });

        vm.remove = function (article) {
            $confirm({title: 'Удаление статьи', text: 'Вы действительно хотите удалить статью?'})
                .then(function(){
                    Article
                        .remove(article.id)
                        .then(function (response) {
                            Notification.success('Статья успешно удалена');
                            vm.table.reload();
                        });
                });
        };

        return this;
    }

})();