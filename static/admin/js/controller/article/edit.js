(function() {
    'use strict';

    angular
        .module('admin')
        .controller('ArticleEditController', controller);

    function controller(
        $rootScope,
        Article,
        $stateParams,
        $state,
        Notification,
        Upload
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Редактирование статьи';

        vm.article = {};

        Article
            .get($stateParams.id)
            .then(function (response) {
                vm.article = response.data;
            });

        vm.save = function () {
            Article
                .update(vm.article)
                .then(function (response) {
                    Notification.success('Статья успешно обновлена');
                    $state.go('articles');
                })
        };

        vm.imageUpload = function(files) {
            if (files != null) {

                Upload.upload({
                    url: 'api/admin/articles/' + $stateParams.id +'/upload/editor-image',
                    file: files[0]
                }).success(function(data, status, headers, config) {
                    $('.summernote').summernote('editor.insertImage', '/uploads/articles/' + $stateParams.id +'/' + data.image);
                });

            }
        };


        return this;
    }

})();