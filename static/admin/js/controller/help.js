(function() {
    'use strict';

    angular
        .module('admin')
        .controller('HelpController', controller);

    function controller($rootScope) {
        var vm = this;

        $rootScope.pageTitle = 'Помощь';

        return this;
    }

})();