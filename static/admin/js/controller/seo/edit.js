(function() {
    'use strict';

    angular
        .module('admin')
        .controller('PageEditController', controller);

    function controller($rootScope, Seo, $stateParams, Notification, $state) {
        var vm = this;

        vm.page = {};

        $rootScope.pageTitle = 'Редактирование страницы';

        Seo
            .get($stateParams.id)
            .then(function (response) {
                vm.page = response.data;
            });

        vm.save = function () {
            Seo
                .update(vm.page)
                .then(function (response) {
                    Notification.success('Страница успешно обновлена');
                    $state.go('seo')
                })
        };


        return this;
    }

})();