(function() {
    'use strict';

    angular
        .module('admin')
        .controller('SeoController', controller);

    function controller($rootScope, Seo) {
        var vm = this;

        vm.pages = [];

        $rootScope.pageTitle = 'Страницы';

        Seo
            .all()
            .then(function (response) {
                vm.pages = response.data;
            });


        return this;
    }

})();