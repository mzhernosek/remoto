(function() {
    'use strict';

    angular
        .module('admin')
        .controller('CategoryAddController', controller);

    function controller(
        $rootScope,
        Upload,
        Notification,
        Category,
        $state
    ) {
        var vm = this;

        vm.category = {};
        vm.file = null;

        vm.check = function ($files, $file, $newFiles, $duplicateFiles, $invalidFiles) {

            if ($invalidFiles.length > 0) {
                Notification.error('Выберите файл необходимого размера. 126 X 126 пикселей');
            }
        };

        vm.save = function () {

            if (vm.file) {
                Category
                    .add(vm.category)
                    .then(function (response) {

                        console.log(response);

                        Upload.upload({
                            url: 'api/admin/categories/'+response.data.id+'/image',
                            data: {file: vm.file}
                        }).then(function (resp) {
                            $state.go('options_categories');
                        }, function (resp) {
                            console.log('Error status: ' + resp.status);
                        }, function (evt) {
                            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                        });
                    });
            } else {
                Notification.error('Выберите файл');
            }
        };

        $rootScope.pageTitle = 'Добавление категории';

        return this;
    }

})();