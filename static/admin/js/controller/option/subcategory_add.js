(function() {
    'use strict';

    angular
        .module('admin')
        .controller('SubcategoryAddController', controller);

    function controller(
        $rootScope,
        Notification,
        Category,
        $state,
        $stateParams
    ) {
        var vm = this;

        vm.category = {
            parent: $stateParams.id
        };


        vm.save = function () {
            Category
                .add(vm.category)
                .then(function (response) {
                    Notification.success('Подкатегория успешно добавлена');
                    $state.go('subcategories', {id:$stateParams.id});
                });
        };

        $rootScope.pageTitle = 'Добавление подкатегории';

        return this;
    }

})();