(function() {
    'use strict';

    angular
        .module('admin')
        .controller('OptionFunctionalController', controller);

    function controller($rootScope, Option, Notification) {
        var vm = this;

        vm.options = [];

        $rootScope.pageTitle = 'Настройки функционала';

        Option
            .all()
            .then(function (response) {
                vm.options = response.data;
            });

        vm.save = function (option) {
            Option
                .update(option)
                .then(function (response) {
                    Notification.success('Настройка успешна обновлена');
                });
        };

        return this;
    }

})();