(function() {
    'use strict';

    angular
        .module('admin')
        .controller('SubcategoryEditController', controller);

    function controller(
        $rootScope,
        Upload,
        Notification,
        Category,
        $state,
        $stateParams
    ) {
        var vm = this;

        vm.category = {};

        $rootScope.pageTitle = 'Редактирование подкатегории';

        Category
            .get($stateParams.id)
            .then(function (response) {
                vm.category = response.data;

            });



        vm.save = function () {
            Category
                .update(vm.category)
                .then(function (response) {
                    $state.go('subcategories', {id:vm.category.parent});
                })
        };







        return this;
    }

})();