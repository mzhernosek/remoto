(function() {
    'use strict';

    angular
        .module('admin')
        .controller('OptionCategoryController', controller);

    function controller(
        $rootScope,
        Category,
        Notification
    ) {
        var vm = this;

        vm.categories = [];

        $rootScope.pageTitle = 'Категории';

        Category
            .all()
            .then(function (response) {
                vm.categories = response.data;
            });

        vm.delete = function (category) {
            Category
                .delete(category)
                .then(function (response) {
                    vm.categories.splice(vm.categories.indexOf(category), 1);
                    Notification.success('Категория успешно удалена');
                })
        };

        return this;
    }

})();