(function() {
    'use strict';

    angular
        .module('admin')
        .controller('CategoryEditController', controller);

    function controller(
        $rootScope,
        Upload,
        Notification,
        Category,
        $state,
        $stateParams
    ) {
        var vm = this;

        vm.category = {};

        $rootScope.pageTitle = 'Редактирование категории';

        Category
            .get($stateParams.id)
            .then(function (response) {
                vm.category = response.data;
                vm.file = '/uploads/services/'+vm.category.image;
            });

        vm.save = function () {
            Category
                .update(vm.category)
                .then(function (response) {
                    if (vm.file != '/uploads/services/'+response.data.image) {
                        Upload.upload({
                            url: 'api/admin/categories/'+vm.category.id+'/image',
                            data: {file: vm.file}
                        }).then(function (resp) {
                            $state.go('options_categories');
                        }, function (resp) {
                            console.log('Error status: ' + resp.status);
                        }, function (evt) {
                            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            document.getElementById('progress').style.width = progressPercentage+'%';
                        });
                    } else {
                        $state.go('options_categories')
                    }
                })
        };

        vm.check = function ($files, $file, $newFiles, $duplicateFiles, $invalidFiles) {

            if ($invalidFiles.length > 0) {
                Notification.error('Выберите файл необходимого размера. 126 X 126 пикселей');
            }
        };

        return this;
    }

})();