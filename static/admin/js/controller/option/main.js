(function() {
    'use strict';

    angular
        .module('admin')
        .controller('OptionMainController', controller);

    function controller(
        $rootScope,
        Option,
        Upload,
        Notification
    ) {
        var vm = this;
        $rootScope.pageTitle = 'Настройки главной страницы';

        vm.video = {};
        vm.images = [];
        vm.image = {};

        Option
            .getByKey('main_video')
            .then(function (response) {
                vm.video = response.data;
            });

        Option
            .getByKey('main_images')
            .then(function (response) {
                vm.image = response.data;
                vm.images = vm.image.value.split(',');
            });

        vm.upload = function (file, errFile) {
            if (file) {
                Upload.upload({
                    url: 'api/admin/options/image',
                    data: {file: file}
                }).then(function (resp) {
                    vm.images.push(resp.data.file);
                    vm.image.value = vm.images.join(',');
                    Option
                        .update(vm.image)
                        .then(function (response) {
                            Notification.success('Изображение успешно добавлено');
                        })
                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                }, function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            }

        };

        vm.removeFile = function (file) {
            Option
                .removeImage(file)
                .then(function (response) {
                    vm.images.splice(vm.images.indexOf(file));
                    vm.image.value = vm.images.join(',');
                    Option
                        .update(vm.image)
                        .then(function (response) {
                            Notification.success('Изображение успешно удалено');

                        })
                })
        };

        vm.save = function () {
            Option
                .update(vm.video)
                .then(function (response) {

                });

        };

        return this;
    }

})();