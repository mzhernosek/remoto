(function() {
    'use strict';

    angular
        .module('admin')
        .controller('OptionIndexController', controller);

    function controller($rootScope) {
        var vm = this;

        $rootScope.pageTitle = 'Настройки';

        return this;
    }

})();