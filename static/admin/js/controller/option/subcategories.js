(function() {
    'use strict';

    angular
        .module('admin')
        .controller('SubcategoryController', controller);

    function controller(
        $rootScope,
        Category,
        Notification,
        $stateParams
    ) {
        var vm = this;

        vm.categories = [];

        vm.parentId = $stateParams.id;

        $rootScope.pageTitle = 'Подкатегории';

        Category
            .getSubcategories($stateParams.id)
            .then(function (response) {
                vm.categories = response.data;
            });

        vm.delete = function (category) {
            Category
                .delete(category)
                .then(function (response) {
                    vm.categories.splice(vm.categories.indexOf(category), 1);
                    Notification.success('Категория успешно удалена');
                })
        };

        return this;
    }

})();