(function() {
    'use strict';

    angular
        .module('admin')
        .controller('LogsController', controller);

    function controller(
        $rootScope
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Логи';

        return this;
    }

})();