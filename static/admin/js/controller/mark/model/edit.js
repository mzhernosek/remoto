(function() {
    'use strict';

    angular
        .module('admin')
        .controller('ModelEditController', controller);

    function controller(
        $rootScope,
        Model,
        $stateParams,
        $state,
        Notification
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Редактирование модели';

        vm.model = {};

        Model
            .get($stateParams.id)
            .then(function (response) {
                vm.model = response.data;
            });

        vm.save = function () {
            Model
                .update(vm.model)
                .then(function (response) {
                    Notification.success('Модель успешно обновлена');
                    $state.go('models', {id: vm.model.mark.id});
                })
        };


        return this;
    }

})();