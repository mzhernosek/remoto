(function() {
    'use strict';

    angular
        .module('admin')
        .controller('ModelAddController', controller);

    function controller(
        $rootScope,
        Model,
        $stateParams,
        $state,
        Notification
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Добавление модели';

        vm.markId = $stateParams.id;
        vm.model = {};

        vm.save = function () {
            Model
                .add(vm.markId, vm.model)
                .then(function (response) {
                    Notification.success('Модель успешно добавлена');
                    $state.go('models', {id:vm.markId});
                })
        };


        return this;
    }

})();