(function() {
    'use strict';

    angular
        .module('admin')
        .controller('ModelsController', controller);

    function controller(
        $rootScope,
        Model,
        NgTableParams,
        $confirm,
        Notification,
        $stateParams
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Модели';

        vm.markId = $stateParams.id;

        vm.table = new NgTableParams({}, {
            getData: function(params) {
                return Model.all(vm.markId, params.url()).then(function(data) {
                    params.total(data.data.count);
                    return data.data.data;
                });
            }
        });

        vm.remove = function (model) {
            $confirm({title: 'Удаление модели', text: 'Вы действительно хотите удалить модель?'})
                .then(function(){
                    Model
                        .remove(model.id)
                        .then(function (response) {
                            Notification.success('Модель успешно удалена');
                            vm.table.reload();
                        });
                });
        };

        return this;
    }

})();