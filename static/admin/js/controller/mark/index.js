(function() {
    'use strict';

    angular
        .module('admin')
        .controller('MarksController', controller);

    function controller(
        $rootScope,
        Mark,
        NgTableParams,
        $confirm,
        Notification
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Марки';

        vm.table = new NgTableParams({}, {
            getData: function(params) {
                return Mark.all(params.url()).then(function(data) {
                    params.total(data.data.count);
                    return data.data.data;
                });
            }
        });

        vm.remove = function (mark) {
            $confirm({title: 'Удаление марки', text: 'Вы действительно хотите удалить марку?'})
                .then(function(){
                    Mark
                        .remove(mark.id)
                        .then(function (response) {
                            Notification.success('Марка успешно удалена');
                            vm.table.reload();
                        });
                });
        };

        return this;
    }

})();