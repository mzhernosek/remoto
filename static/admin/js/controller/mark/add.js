(function() {
    'use strict';

    angular
        .module('admin')
        .controller('MarkAddController', controller);

    function controller(
        $rootScope,
        Mark,
        $stateParams,
        $state,
        Notification
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Добавление марки';

        vm.mark = {};

        vm.save = function () {
            Mark
                .add(vm.mark)
                .then(function (response) {
                    Notification.success('Марка успешно добавлена');
                    $state.go('marks');
                })
        };


        return this;
    }

})();