(function() {
    'use strict';

    angular
        .module('admin')
        .controller('MarkEditController', controller);

    function controller(
        $rootScope,
        Mark,
        $stateParams,
        $state,
        Notification
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Редактирование марки';

        vm.mark = {};

        Mark
            .get($stateParams.id)
            .then(function (response) {
               vm.mark = response.data;
            });

        vm.save = function () {
            Mark
                .update(vm.mark)
                .then(function (response) {
                    Notification.success('Марка успешно обновлена');
                    $state.go('marks');
                })
        };


        return this;
    }

})();