(function() {
    'use strict';

    angular
        .module('admin')
        .controller('UsersController', controller);

    function controller(
        $rootScope,
        NgTableParams,
        User
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Пользователи';

        vm.table = new NgTableParams({}, {
            getData: function(params) {
                return User.all(params.url()).then(function(data) {
                    params.total(data.data.count);
                    return data.data.data;
                });
            }
        });

        return this;
    }

})();