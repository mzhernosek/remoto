(function() {
    'use strict';

    angular
        .module('admin')
        .controller('UserPageController', controller);

    function controller(
        $rootScope,
        Region,
        $scope,
        Sto,
        $state,
        Notification,
        $stateParams,
        Billing,
        NgTableParams,
        Upload
    ) {
        var vm = this,
            userId = $stateParams.id;


        $rootScope.pageTitle = 'Страница СТО';

        vm.user = {};
        vm.payment = {};
        vm.regions = [];
        vm.cities = [];

        Sto
            .find($stateParams.id)
            .then(function (response) {
                vm.user = response.data;
            });



        Region
            .all()
            .then(function (response) {
                vm.regions = response.data.data;
                //vm.sto.region = vm.regions[0];
            });

        $scope.$watch("vm.sto.region", function (region) {
            if (region) {
                Region
                    .allCities(region.id)
                    .then(function (response) {
                        vm.cities = response.data.data;
                        //vm.sto.city = vm.cities[0];
                    })
            }
        });



        return this;
    }

})();