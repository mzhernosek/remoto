(function() {
    'use strict';

    angular
        .module('admin')
        .controller('ComponentsController', controller);

    function controller($rootScope) {
        var vm = this;

        $rootScope.pageTitle = 'Компоненты';


        return this;
    }

})();