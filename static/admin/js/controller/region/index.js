(function() {
    'use strict';

    angular
        .module('admin')
        .controller('RegionsController', controller);

    function controller(
        $rootScope,
        Region,
        NgTableParams
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Регионы';

        vm.table = new NgTableParams({}, {
            getData: function(params) {
                return Region.all(params.url()).then(function(data) {
                    params.total(data.data.count);
                    return data.data.data;
                });
            }
        });

        return this;
    }

})();