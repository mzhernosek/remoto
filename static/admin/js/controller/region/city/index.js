(function() {
    'use strict';

    angular
        .module('admin')
        .controller('CitiesController', controller);

    function controller(
        $rootScope,
        Region,
        NgTableParams,
        $stateParams
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Города';

        vm.regionId = $stateParams.id;

        vm.table = new NgTableParams({}, {
            getData: function(params) {
                return Region.allCities($stateParams.id, params.url()).then(function(data) {
                    params.total(data.data.count);
                    return data.data.data;
                });
            }
        });

        return this;
    }

})();