(function() {
    'use strict';

    angular
        .module('admin')
        .controller('CityAddController', controller);

    function controller(
        $rootScope,
        Region,
        $state,
        $stateParams
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Добавление города';

        vm.city = {};

        vm.save = function () {
            Region
                .addCity($stateParams.id, vm.city)
                .then(function () {
                    $state.go('cities', {id:$stateParams.id});
                });
        };


        return this;
    }

})();