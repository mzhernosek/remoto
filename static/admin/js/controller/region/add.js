(function() {
    'use strict';

    angular
        .module('admin')
        .controller('RegionAddController', controller);

    function controller(
        $rootScope,
        Region,
        $state
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Добавление региона';

        vm.region = {};

        vm.save = function () {
            Region
                .add(vm.region)
                .then(function () {
                   $state.go('regions');
                });
        };


        return this;
    }

})();