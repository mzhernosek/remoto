(function() {
    'use strict';

    angular
        .module('admin')
        .controller('ContentController', controller);

    function controller($rootScope) {
        var vm = this;

        $rootScope.pageTitle = 'Контент';

        return this;
    }

})();