(function() {
    'use strict';

    angular
        .module('admin')
        .controller('ContentReviewAddController', controller);

    function controller(
        $rootScope,
        Upload,
        Notification,
        Review,
        $state
    ) {
        var vm = this;

        vm.review = {};
        vm.file = null;

        vm.check = function ($files, $file, $newFiles, $duplicateFiles, $invalidFiles) {

            if ($invalidFiles.length > 0) {
                Notification.error('Выберите файл необходимого размера. 86 X 86 пикселей');
            }
        };

        vm.save = function () {

            if (vm.file) {
                Review
                    .add(vm.review)
                    .then(function (response) {
                        Upload.upload({
                            url: 'api/admin/reviews/'+response.data.id+'/image',
                            data: {file: vm.file}
                        }).then(function (resp) {
                            $state.go('content_reviews');
                        }, function (resp) {
                            console.log('Error status: ' + resp.status);
                        }, function (evt) {
                            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                        });
                    });
            } else {
                Notification.error('Выберите файл');
            }
        };

        $rootScope.pageTitle = 'Добавление отзыва';

        return this;
    }

})();