(function() {
    'use strict';

    angular
        .module('admin')
        .controller('ContentReviewsController', controller);

    function controller(
        $rootScope,
        Review,
        Notification
    ) {
        var vm = this;

        vm.reviews = [];

        $rootScope.pageTitle = 'Отзывы';

        Review
            .all()
            .then(function (response) {
                vm.reviews = response.data;
            });

        vm.delete = function (review) {
            Review
                .delete(review)
                .then(function (response) {
                    vm.reviews.splice(vm.reviews.indexOf(review), 1);
                    Notification.success('Отзыв успешо удален');
                })
        };

        return this;
    }

})();