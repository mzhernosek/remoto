(function() {
    'use strict';

    angular
        .module('admin')
        .controller('ContentReviewEditController', controller);

    function controller(
        $rootScope,
        Upload,
        Notification,
        Review,
        $state,
        $stateParams
    ) {
        var vm = this;

        vm.review = {};

        $rootScope.pageTitle = 'Редактирование отзыва';

        Review
            .get($stateParams.id)
            .then(function (response) {
                vm.review = response.data;
                vm.file = '/uploads/reviews/'+vm.review.image;
            });

        vm.save = function () {
            Review
                .update(vm.review)
                .then(function (response) {
                    if (vm.file != '/uploads/reviews/'+response.data.image) {
                        Upload.upload({
                            url: 'api/admin/reviews/'+vm.review.id+'/image',
                            data: {file: vm.file}
                        }).then(function (resp) {
                            $state.go('content_reviews');
                        }, function (resp) {
                            console.log('Error status: ' + resp.status);
                        }, function (evt) {
                            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                        });
                    } else {
                        $state.go('content_reviews')
                    }
                })
        };





        vm.check = function ($files, $file, $newFiles, $duplicateFiles, $invalidFiles) {

            if ($invalidFiles.length > 0) {
                Notification.error('Выберите файл необходимого размера. 86 X 86 пикселей');
            }
        };

        return this;
    }

})();