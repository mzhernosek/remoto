(function() {
    'use strict';

    angular
        .module('admin')
        .controller('StoPageController', controller);

    function controller(
        $rootScope,
        Region,
        $scope,
        Sto,
        $state,
        Notification,
        $stateParams,
        Billing,
        NgTableParams,
        Upload,
        $timeout
    ) {
        var vm = this,
            stoId = $stateParams.id,
            lat = 55.18,
            lon = 30.18;


        $rootScope.pageTitle = 'Страница СТО';



        vm.sto = {

        };
        vm.sto.description = '';

        vm.payment = {};
        vm.regions = [];
        vm.cities = [];

        vm.imageUpload = function(files) {
            if (files != null) {

                Upload.upload({
                    url: 'api/admin/sto/' + $stateParams.id +'/upload/editor-image',
                    file: files[0]
                }).success(function(data, status, headers, config) {
                    $('.summernote').summernote('editor.insertImage', '/uploads/sto/' + $stateParams.id +'/' + data.image);
                });

            }
        };



        Sto
            .find($stateParams.id)
            .then(function (response) {
                vm.sto = response.data;

                if (!vm.sto.lat) {
                    vm.sto.lat = lat;
                    vm.sto.lon = lon;
                }


                ymaps.ready(function () {
                    var myMap = new ymaps.Map('map', {
                        center: [vm.sto.lat, vm.sto.lon],
                        zoom: 14
                    });

                    var myPlacemark = new ymaps.Placemark(myMap.getCenter());

                    myMap.events.add('click', function (e) {
                        var coords = e.get('coordPosition');
                        myMap.setCenter(coords);


                        myPlacemark.geometry.setCoordinates(coords);
                        vm.sto.lat = coords[0];
                        vm.sto.lon = coords[1];
                    });

                    myMap.geoObjects.add(myPlacemark);
                    myMap.behaviors.enable('scrollZoom');
                });
            });

        vm.tablePayments = new NgTableParams({}, {
            getData: function(params) {
                return Billing.getPaymentsBySto(stoId, params.url()).then(function(data) {
                    params.total(data.data.count);
                    return data.data.data;
                });
            }
        });

        Region
            .all()
            .then(function (response) {
                vm.regions = response.data.data;
                //vm.sto.region = vm.regions[0];
            });

        $scope.$watch("vm.sto.region", function (region) {
            if (region) {
                Region
                    .allCities(region.id)
                    .then(function (response) {
                        vm.cities = response.data.data;
                        //vm.sto.city = vm.cities[0];
                    })
            }
        });

        vm.save = function () {
            Sto
                .update(vm.sto)
                .then(function (response) {
                    Notification.success('СТО успешно обновлено');
                });
        };

        vm.addPayment = function () {
            Billing
                .addPayment(vm.sto.id, vm.payment)
                .then(function (response) {
                    vm.tablePayments.reload();
                    Notification.success('Платеж успешно добавлен');
                });

        };

        vm.uploadImage = function (file) {
            if (file) {
                Upload.upload({
                    url: 'api/admin/sto/' + $stateParams.id +'/upload/image',
                    file: file
                }).then(function (resp) {
                    console.log(resp);
                    vm.sto.image = '/uploads/sto/' + $stateParams.id + '/' + resp.data.image;
                }, function (resp) {

                }, function (evt) {


                });
            }

        };

        vm.uploadImages = function (files) {
            console.log(files);
            if (files) {
                Upload.upload({
                    url: 'api/admin/sto/' + $stateParams.id +'/upload/images',
                    data: {files: files}
                }).then(function (resp) {
                    console.log(resp);
                    vm.sto.images = resp.data.images;
                }, function (resp) {

                }, function (evt) {


                });
            }

        };

        return this;
    }

})();