(function() {
    'use strict';

    angular
        .module('admin')
        .controller('StoAddController', controller);

    function controller(
        $rootScope,
        Region,
        $scope,
        Sto,
        $state,
        Notification
    ) {
        var vm = this,
            lat = 55.18,
            lon = 30.18;


        $rootScope.pageTitle = 'Добавление СТО';

        vm.sto = {
            lat: lat,
            lon: lon
        };

        vm.regions = [];
        vm.cities = [];

        Region
            .all()
            .then(function (response) {
                vm.regions = response.data.data;
                vm.sto.region = vm.regions[0];




            });

        $scope.$watch("vm.sto.region", function (region) {
            if (region) {
                Region
                    .allCities(region.id)
                    .then(function (response) {
                        vm.cities = response.data.data;
                        vm.sto.city = vm.cities[0];
                    })
            }
        });




        vm.save = function () {
            Sto
                .add(vm.sto)
                .then(function (response) {
                    Notification.success('СТО успешно добавлено');
                    $state.go('sto');
                });
        };


        return this;
    }

})();