(function() {
    'use strict';

    angular
        .module('admin')
        .controller('StoController', controller);

    function controller(
        $rootScope,
        NgTableParams,
        Sto
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Сто';

        vm.table = new NgTableParams({}, {
            getData: function(params) {
                return Sto.all(params.url()).then(function(data) {
                    params.total(data.data.count);
                    return data.data.data;
                });
            }
        });


        return this;
    }

})();