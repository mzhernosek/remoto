(function() {
    'use strict';

    angular
        .module('admin')
        .controller('BillingController', controller);

    function controller(
        $rootScope
    ) {
        var vm = this;

        $rootScope.pageTitle = 'Биллинг';

        return this;
    }

})();