(function() {
    'use strict';
    
    angular
        .module('admin', [
            'ui.router',
            'angular-confirm',
            'ng-mfb',
            'angular-ladda',
            'angularScreenfull',
            'ui-notification',
            'xeditable',
            'ngTable',
            'ngFileUpload',
            'summernote'
        ])
        .run(Run)
        .config(Config)
        .filter('pageTitle', pageTitleFilter)
    ;

   
    function Run ($rootScope, $state, $stateParams, editableThemes,editableOptions) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        editableThemes['default'].submitTpl = '<button type="submit" class="btn btn-xs btn-primary"><span class="fa fa-save"></span></button>';
        editableThemes['default'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-xs btn-default"><span class="fa fa-close"></span></button>';
        editableOptions.theme = 'default';


    }

    function Config ($stateProvider, $locationProvider, $httpProvider, $qProvider) {

        $qProvider.errorOnUnhandledRejections(false);

        $locationProvider.html5Mode(true).hashPrefix('/');

        $stateProvider
            .state('index', {
                url: "/admin",
                templateUrl: '/admin-js/templates/index.html',
                controller: 'IndexController',
                controllerAs: 'vm'
            })
            .state('seo', {
                url: "/admin/seo",
                templateUrl: '/admin-js/templates/seo/index.html',
                controller: 'SeoController',
                controllerAs: 'vm'
            })
            .state('page_edit', {
                url: "/admin/page/:id/edit",
                templateUrl: '/admin-js/templates/seo/edit.html',
                controller: 'PageEditController',
                controllerAs: 'vm'
            })
            .state('components', {
                url: "/admin/components",
                templateUrl: '/admin-js/templates/components/index.html',
                controller: 'ComponentsController',
                controllerAs: 'vm'
            })
            .state('users', {
                url: "/admin/users",
                templateUrl: '/admin-js/templates/users/index.html',
                controller: 'UsersController',
                controllerAs: 'vm'
            })
            .state('user_page', {
                url: "/admin/user/:id",
                templateUrl: '/admin-js/templates/users/page.html',
                controller: 'UserPageController',
                controllerAs: 'vm'
            })
            .state('options', {
                url: "/admin/options",
                templateUrl: '/admin-js/templates/option/index.html',
                controller: 'OptionIndexController',
                controllerAs: 'vm'
            })
            .state('options_functional', {
                url: "/admin/options/functional",
                templateUrl: '/admin-js/templates/option/functional.html',
                controller: 'OptionFunctionalController',
                controllerAs: 'vm'
            })
            .state('options_main', {
                url: "/admin/options/main",
                templateUrl: '/admin-js/templates/option/main.html',
                controller: 'OptionMainController',
                controllerAs: 'vm'
            })
            .state('options_categories', {
                url: "/admin/options/categories",
                templateUrl: '/admin-js/templates/option/categories.html',
                controller: 'OptionCategoryController',
                controllerAs: 'vm'
            })
            .state('categories_add', {
                url: "/admin/options/categories/add",
                templateUrl: '/admin-js/templates/option/category_add.html',
                controller: 'CategoryAddController',
                controllerAs: 'vm'
            })
            .state('subcategories', {
                url: "/admin/options/categories/:id/subcategories",
                templateUrl: '/admin-js/templates/option/subcategories.html',
                controller: 'SubcategoryController',
                controllerAs: 'vm'
            })
            .state('subcategory_add', {
                url: "/admin/options/categories/:id/subcategory/add",
                templateUrl: '/admin-js/templates/option/subcategory_add.html',
                controller: 'SubcategoryAddController',
                controllerAs: 'vm'
            })
            .state('subcategory_edit', {
                url: "/admin/options/subcategories/:id/edit",
                templateUrl: '/admin-js/templates/option/subcategory_add.html',
                controller: 'SubcategoryEditController',
                controllerAs: 'vm'
            })
            .state('category_edit', {
                url: "/admin/options/categories/:id/edit",
                templateUrl: '/admin-js/templates/option/category_edit.html',
                controller: 'CategoryEditController',
                controllerAs: 'vm'
            })
            .state('verifications', {
                url: "/admin/verifications",
                templateUrl: '/admin-js/templates/verification/index.html',
                controller: 'VerificationController',
                controllerAs: 'vm'
            })
            .state('orders', {
                url: "/admin/orders",
                templateUrl: '/admin-js/templates/order/index.html',
                controller: 'OrdersController',
                controllerAs: 'vm'
            })
            .state('order_page', {
                url: "/admin/order/:id",
                templateUrl: '/admin-js/templates/order/page.html',
                controller: 'OrderPageController',
                controllerAs: 'vm'
            })
            .state('content', {
                url: "/admin/content",
                templateUrl: '/admin-js/templates/content/index.html',
                controller: 'ContentController',
                controllerAs: 'vm'
            })
            .state('content_reviews', {
                url: "/admin/content/reviews",
                templateUrl: '/admin-js/templates/content/reviews.html',
                controller: 'ContentReviewsController',
                controllerAs: 'vm'
            })
            .state('content_review_add', {
                url: "/admin/content/reviews/add",
                templateUrl: '/admin-js/templates/content/review_add.html',
                controller: 'ContentReviewAddController',
                controllerAs: 'vm'
            })
            .state('content_review_edit', {
                url: "/admin/content/reviews/:id/edit",
                templateUrl: '/admin-js/templates/content/review_edit.html',
                controller: 'ContentReviewEditController',
                controllerAs: 'vm'
            })
            .state('regions', {
                url: "/admin/regions",
                templateUrl: '/admin-js/templates/region/index.html',
                controller: 'RegionsController',
                controllerAs: 'vm'
            })
            .state('regions_add', {
                url: "/admin/regions/add",
                templateUrl: '/admin-js/templates/region/add.html',
                controller: 'RegionAddController',
                controllerAs: 'vm'
            })
            .state('cities', {
                url: "/admin/regions/:id/cities",
                templateUrl: '/admin-js/templates/region/city/index.html',
                controller: 'CitiesController',
                controllerAs: 'vm'
            })
            .state('cities_add', {
                url: "/admin/regions/:id/cities/add",
                templateUrl: '/admin-js/templates/region/city/add.html',
                controller: 'CityAddController',
                controllerAs: 'vm'
            })
            .state('sto', {
                url: "/admin/sto",
                templateUrl: '/admin-js/templates/sto/index.html',
                controller: 'StoController',
                controllerAs: 'vm'
            })
            .state('sto_add', {
                url: "/admin/sto/add",
                templateUrl: '/admin-js/templates/sto/add.html',
                controller: 'StoAddController',
                controllerAs: 'vm'
            })
            .state('sto_page', {
                url: "/admin/sto/:id",
                templateUrl: '/admin-js/templates/sto/page.html',
                controller: 'StoPageController',
                controllerAs: 'vm'
            })
            .state('logs', {
                url: "/admin/logs",
                templateUrl: '/admin-js/templates/logs/index.html',
                controller: 'LogsController',
                controllerAs: 'vm'
            })
            .state('marks', {
                url: "/admin/marks",
                templateUrl: '/admin-js/templates/mark/index.html',
                controller: 'MarksController',
                controllerAs: 'vm'
            })
            .state('mark_edit', {
                url: "/admin/marks/:id/edit",
                templateUrl: '/admin-js/templates/mark/form.html',
                controller: 'MarkEditController',
                controllerAs: 'vm'
            })
            .state('mark_add', {
                url: "/admin/marks/add",
                templateUrl: '/admin-js/templates/mark/form.html',
                controller: 'MarkAddController',
                controllerAs: 'vm'
            })
            .state('billing', {
                url: "/admin/billing",
                templateUrl: '/admin-js/templates/billing/index.html',
                controller: 'BillingController',
                controllerAs: 'vm'
            })
            .state('models', {
                url: "/admin/marks/:id/models",
                templateUrl: '/admin-js/templates/mark/model/index.html',
                controller: 'ModelsController',
                controllerAs: 'vm'
            })
            .state('model_edit', {
                url: "/admin/models/:id/edit",
                templateUrl: '/admin-js/templates/mark/model/form.html',
                controller: 'ModelEditController',
                controllerAs: 'vm'
            })
            .state('model_add', {
                url: "/admin/marks/:id/models/add",
                templateUrl: '/admin-js/templates/mark/model/form.html',
                controller: 'ModelAddController',
                controllerAs: 'vm'
            })
            .state('articles', {
                url: "/admin/articles",
                templateUrl: '/admin-js/templates/article/index.html',
                controller: 'ArticlesController',
                controllerAs: 'vm'
            })
            .state('article_add', {
                url: "/admin/articles/add",
                templateUrl: '/admin-js/templates/article/add.html',
                controller: 'ArticleAddController',
                controllerAs: 'vm'
            })
            .state('article_edit', {
                url: "/admin/article/:id/edit",
                templateUrl: '/admin-js/templates/article/edit.html',
                controller: 'ArticleEditController',
                controllerAs: 'vm'
            })




        ;

        $httpProvider.interceptors.push(function($injector, $q) {
            return {
                'request': function (request) {
                    if (/^api\//.test(request.url)) {
                        request.url = window.base + request.url;
                    }
                    return request;
                },
                'responseError': function(response) {
                    switch (response.status) {
                        case 400:
                            console.log(response.data.data);
                            if (response.data.message) {

                                    $injector.get('Notification').error(response.data.message);

                            } else {
                                angular.forEach(response.data.error.exception, function (el) {
                                    $injector.get('Notification').error(el.message);
                                });
                            }

                            return $q.reject(response);

                            break;
                        case 401:
                            window.localStorage.removeItem('_token');
                            window.localStorage.removeItem('user');
                            //window.location = window.base+'login';
                            return $q.reject(response);
                            break;
                        case 403:
                            $injector.get('Notification').error('Вы не имеете доступа к этому действию.');
                            return $q.reject(response);
                        case 404:
                            break;
                        case 415:
                            console.log(response);
                            return $q.reject(response);
                            break;
                        default:
                            $injector.get('Notification').error('При запросе произошла ошибка.');
                            return $q.reject(response);
                            break;

                    }

                    return $q.reject(response);
                }
            };
        });
    }

    function pageTitleFilter () {
        return function (val) {
            return Page.get(val);
        }
    }

})();
