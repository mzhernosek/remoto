var Page = (function(){

    var titles = {
        'index' : 'Главная страница',
        'login' : 'Вход',
        'account.index' : 'Личный кабинет',
        'account.objects' : 'Объекты',
        'account.cabinet' : 'Мой кабинет',
        'account.my_company' : 'Моя компания',
        'account.social' : 'Интеграция',
        'account.payment' : 'Финансы',
        'account.history' : 'Финансы.История',
        'account.object_add' : 'Добавление объекта',
        'account.object' : 'Страница объекта',
        'account.object_estimates': 'Сметы',
        'account.object_export': 'Экспорт объекта',
        'account.estimate': 'Страница сметы',
        'account.estimate_works': 'Расценки',
        'account.estimate_acts': 'Акты выполненных работ',
        'account.digest': 'Персональные справочники',
        'account.digest_system': 'Системные справочники'
    };

    return {
        get: function (key) {
            return titles[key] || key;
        }
    }
}());