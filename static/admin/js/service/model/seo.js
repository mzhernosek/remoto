(function() {
    'use strict';

    angular
        .module('admin')
        .factory('Seo', service);

    function service ($http) {

        return {
            all: all,
            update: update,
            get: get
        };

        function all () {
            return $http.get('api/admin/seo/all');
        }

        function get(id) {
            return $http.get('api/admin/seo/'+id);
        }

        function update (seo) {
            return $http.put('api/admin/seo/'+seo.id, seo);
        }
    }

})();
