(function() {
    'use strict';

    angular
        .module('admin')
        .factory('Order', service);

    function service ($http) {

        return {
            all: all,
            find: find,
            getAnswers: getAnswers,
            update: update
        };

        function all () {
            return $http.get('api/admin/orders');
        }

        function find (id) {
            return $http.get('api/admin/orders/' + id);
        }

        function getAnswers (id) {
            return $http.get('api/admin/orders/' + id + '/answers');
        }

        function update (order) {
            return $http.put('api/admin/orders/' + order.id, order);
        }
    }
})();
