(function() {
    'use strict';

    angular
        .module('admin')
        .factory('Option', service);

    function service ($http) {

        return {
            all: all,
            update: update,
            getByKey: getByKey,
            removeImage: removeImage
        };

        function getByKey(key) {
            return $http.get('api/admin/options/'+key);
        }

        function all () {
            return $http.get('api/admin/options');
        }

        function update (option) {
            return $http.put('api/admin/options/'+option.id, option);
        }

        function removeImage(image)
        {
            return $http.put('api/admin/options/image/'+image);
        }
    }

})();
