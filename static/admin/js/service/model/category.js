(function() {
    'use strict';

    angular
        .module('admin')
        .factory('Category', service);

    function service ($http) {

        return {
            all: all,
            add: add,
            get: get,
            update: update,
            delete: remove,
            getSubcategories: getSubcategories
        };

        function getSubcategories(categoryId) {
            return $http.get('api/admin/categories/'+categoryId+'/subcategories');
        }

        function remove (category) {
            return $http.delete('api/admin/categories/'+category.id);
        }

        function update (category) {
            return $http.put('api/admin/categories/'+category.id, category);
        }

        function get (id) {
            return $http.get('api/admin/categories/'+id);
        }

        function all () {
            return $http.get('api/admin/categories');
        }

        function add (category) {
            return $http.post('api/admin/categories', category);
        }
    }
})();
