(function() {
    'use strict';

    angular
        .module('admin')
        .factory('Review', service);

    function service ($http) {

        return {
            all: all,
            add: add,
            get: get,
            update: update,
            delete: remove
        };

        function remove (review) {
            return $http.delete('api/admin/reviews/'+review.id);
        }

        function update (review) {
            return $http.put('api/admin/reviews/'+review.id, review);
        }

        function get (id) {
            return $http.get('api/admin/reviews/'+id);
        }

        function all () {
            return $http.get('api/admin/reviews');
        }

        function add (review) {
            return $http.post('api/admin/reviews', review);
        }
    }
})();
