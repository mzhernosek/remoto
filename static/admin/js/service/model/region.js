(function() {
    'use strict';

    angular
        .module('admin')
        .factory('Region', service);

    function service ($http) {

        return {
            all: all,
            add: add,
            allCities: allCities,
            addCity: addCity
        };

        function remove (review) {
            return $http.delete('api/admin/reviews/'+review.id);
        }

        function update (review) {
            return $http.put('api/admin/reviews/'+review.id, review);
        }

        function get (id) {
            return $http.get('api/admin/reviews/'+id);
        }

        function all () {
            return $http.get('api/admin/regions');
        }

        function allCities (regionId) {
            return $http.get('api/admin/regions/' + regionId + '/cities');
        }

        function add (region) {
            return $http.post('api/admin/regions', region);
        }

        function addCity (regionId, city) {
            return $http.post('api/admin/regions/' + regionId + '/cities', city);
        }
    }
})();
