(function() {
    'use strict';

    angular
        .module('admin')
        .factory('Mark', service);

    function service ($http) {

        return {
            all: all,
            add: add,
            remove: remove,
            update: update,
            get: get
        };

        function remove (id) {
            return $http.delete('api/admin/marks/'+id);
        }

        function update (mark) {
            return $http.put('api/admin/marks/'+mark.id, mark);
        }

        function get (id) {
            return $http.get('api/admin/marks/'+id);
        }

        function all (params) {
            var count = params ? params.count : 0;
            var page = params ? params.page : 0;

            return $http.get('ajax/marks', {params: {
                count:count, page:page
            }});
        }

        function add (mark) {
            return $http.post('api/admin/marks', mark);
        }
    }
})();
