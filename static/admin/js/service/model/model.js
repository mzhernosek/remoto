(function() {
    'use strict';

    angular
        .module('admin')
        .factory('Model', service);

    function service ($http) {

        return {
            all: all,
            add: add,
            remove: remove,
            update: update,
            get: get
        };

        function remove (id) {
            return $http.delete('api/admin/models/'+id);
        }

        function update (model) {
            return $http.put('api/admin/models/'+model.id, model);
        }

        function get (id) {
            return $http.get('api/admin/models/'+id);
        }

        function all (markId, params) {
            var count = params ? params.count : 0;
            var page = params ? params.page : 0;

            return $http.get('ajax/marks/' + markId + '/models', {params: {
                count:count, page:page
            }});
        }

        function add (markId, model) {
            return $http.post('api/admin/marks/'+markId+'/models', model);
        }
    }
})();
