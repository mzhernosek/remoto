(function() {
    'use strict';

    angular
        .module('admin')
        .factory('Sto', service);

    function service ($http) {

        return {
            add: add,
            all: all,
            find: find,
            update: update
        };

        function add (sto) {
            return $http.post('api/admin/sto', sto);
        }

        function all (params) {
            return $http.get('api/admin/sto', {params: {
                count:params.count, page:params.page
            }});
        }

        function find (id) {
            return $http.get('api/admin/sto/' + id);
        }

        function update (sto) {
            return $http.put('api/admin/sto/' + sto.id, sto);
        }
    }

})();
