(function() {
    'use strict';

    angular
        .module('admin')
        .factory('Article', service);

    function service ($http) {

        return {
            all: all,
            add: add,
            remove: remove,
            update: update,
            get: get
        };

        function remove (id) {
            return $http.delete('api/admin/articles/'+id);
        }

        function update (article) {
            return $http.put('api/admin/articles/'+article.id, article);
        }

        function get (id) {
            return $http.get('api/admin/articles/'+id);
        }

        function all (params) {
            var count = params ? params.count : 0;
            var page = params ? params.page : 0;

            return $http.get('api/admin/articles', {params: {
                count:count, page:page
            }});
        }

        function add (article) {
            return $http.post('api/admin/articles', article);
        }
    }
})();
