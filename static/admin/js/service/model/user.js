(function() {
    'use strict';

    angular
        .module('admin')
        .factory('User', service);

    function service ($http) {

        return {
            all: all
        };

        function all (params) {
            return $http.get('api/admin/users/all', {params: {
                count:params.count, page:params.page
            }});
        }
    }

})();
