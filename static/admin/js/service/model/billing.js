(function() {
    'use strict';

    angular
        .module('admin')
        .factory('Billing', service);

    function service ($http) {

        return {
            addPayment: addPayment,
            getPaymentsBySto: getPaymentsBySto
        };

        function addPayment (stoId, payment) {
            return $http.post('api/admin/sto/' + stoId + '/payments', payment);
        }

        function getPaymentsBySto (stoId, params) {
            return $http.get('api/admin/sto/' + stoId + '/payments', {
                params: {
                    count:params.count, page:params.page
                }
            });
        }
    }
})();
