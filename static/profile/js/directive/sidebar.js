(function() {
    'use strict';

    angular
        .module('app')
        .directive('sidebarCollapse', SidebarDirective);

    function SidebarDirective() {
        var directive = {
            restrict: 'EA',
            replace: false,
            transclude: false,
            link: function(scope, elem, attrs) {
                elem.bind('click', function(){
                    var body = document.getElementsByTagName('body')[0];
                    body.classList.toggle('sidebar-collapse');
                })

            }
        };

        return directive;
    }

})();