(function() {
    'use strict';
    
    angular
        .module('app', [
            'ui.router',
            'angular-confirm',
            'ng-mfb',
            'angular-ladda',
            'angularScreenfull',
            'ui-notification'
        ])
        .run(Run)
        .config(Config)
        .filter('pageTitle', pageTitleFilter)
    ;
   
    function Run ($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    }

    function Config ($stateProvider, $locationProvider, $httpProvider) {
        $locationProvider.html5Mode(true).hashPrefix('/');

        $stateProvider
            .state('index', {
                url: "/profile",
                templateUrl: '/app-js/templates/index.html',
                controller: 'IndexController',
                controllerAs: 'vm'
            })
            .state('task_add', {
                url: "/profile/task/add",
                templateUrl: '/app-js/templates/task/add.html',
                controller: 'TaskAddController',
                controllerAs: 'vm'
            })
            .state('tasks_history', {
                url: "/profile/task/history",
                templateUrl: '/app-js/templates/task/history.html',
                controller: 'TaskHistoryController',
                controllerAs: 'vm'
            })
            .state('accounts', {
                url: "/profile/accounts",
                templateUrl: '/app-js/templates/account/index.html',
                controller: 'AccountsController',
                controllerAs: 'vm'
            })
            .state('others', {
                url: "/profile/others",
                templateUrl: '/app-js/templates/other/index.html',
                controller: 'OthersController',
                controllerAs: 'vm'
            })
        ;

        $httpProvider.interceptors.push(function($injector, $q) {
            return {
                'request': function (request) {
                    if (/^api\//.test(request.url)) {
                        request.url = window.base + request.url;
                    }
                    return request;
                },
                'responseError': function(response) {
                    switch (response.status) {
                        case 400:
                            console.log(response.data.data);
                            if (response.data.message) {

                                    $injector.get('Notification').error(response.data.message);

                            } else {
                                angular.forEach(response.data.error.exception, function (el) {
                                    $injector.get('Notification').error(el.message);
                                });
                            }

                            return $q.reject(response);

                            break;
                        case 401:
                            window.localStorage.removeItem('_token');
                            window.localStorage.removeItem('user');
                            //window.location = window.base+'login';
                            return $q.reject(response);
                            break;
                        case 403:
                            $injector.get('Notification').error('Вы не имеете доступа к этому действию.');
                            return $q.reject(response);
                        case 415:
                            console.log(response);
                            return $q.reject(response);
                            break;
                        default:
                            $injector.get('Notification').error('При запросе произошла ошибка.');
                            return $q.reject(response);
                            break;

                    }

                    return $q.reject(response);
                }
            };
        });
    }

    function pageTitleFilter () {
        return function (val) {
            return Page.get(val);
        }
    }

})();

var dateFormat = function () {
    var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var	_ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d:    d,
                dd:   pad(d),
                ddd:  dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m:    m + 1,
                mm:   pad(m + 1),
                mmm:  dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy:   String(y).slice(2),
                yyyy: y,
                h:    H % 12 || 12,
                hh:   pad(H % 12 || 12),
                H:    H,
                HH:   pad(H),
                M:    M,
                MM:   pad(M),
                s:    s,
                ss:   pad(s),
                l:    pad(L, 3),
                L:    pad(L > 99 ? Math.round(L / 10) : L),
                t:    H < 12 ? "a"  : "p",
                tt:   H < 12 ? "am" : "pm",
                T:    H < 12 ? "A"  : "P",
                TT:   H < 12 ? "AM" : "PM",
                Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default":      "ddd mmm dd yyyy HH:MM:ss",
    shortDate:      "m/d/yy",
    mediumDate:     "mmm d, yyyy",
    longDate:       "mmmm d, yyyy",
    fullDate:       "dddd, mmmm d, yyyy",
    shortTime:      "h:MM TT",
    mediumTime:     "h:MM:ss TT",
    longTime:       "h:MM:ss TT Z",
    isoDate:        "yyyy-mm-dd",
    isoTime:        "HH:MM:ss",
    isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
    ]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
};

Object.prototype.getIndex = function (object) {
    for(var i = 0, len = this.length; i < len; i++) {
        if (this[i] === object) return i;
    }
    return null;
};

Object.prototype.toArray = function () {
    var Ar = [];

    for (var el in this) {
        if (this.hasOwnProperty(el)) {
            Ar.push(this[el]);
        }
    }

    return Ar;
};