<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\City;
use AppBundle\Entity\User;
use AppBundle\Form\StoType;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class FrontController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {
        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);
        $marks = $this->getDoctrine()->getRepository('AppBundle:CarMark')->findBy([], ['title' => 'asc']);
        $newOrders = $this->getDoctrine()->getRepository('AppBundle:Order')->findBy(
            [],
            ['date_created' => 'desc'],
            3
        );
        $newSto = $this->getDoctrine()->getRepository('AppBundle:User')->findBy(
            ['type' => User::PROFILE_TYPE_STO, 'published' => true],
            ['id' => 'desc'],
            3
        );

        return $this->render('front/index.html.twig', [
            'marks' => $marks,
            'form' => $form->createView(),
            'newOrders' => $newOrders,
            'newSto' => $newSto
        ]);
    }

    /**
     * @Route("/sto/{id}-{slug}", name="sto_page", requirements={"id": "\d+"}, defaults={"slug": ""})
     * @ParamConverter("sto", class="AppBundle:User")
     */
    public function stoPageAction(User $sto)
    {

        return $this->render('front/sto_page.html.twig', [
            'sto' => $sto
        ]);
    }

    /**
     * @Route("/all-sto", name="all_sto")
     */
    public function allStoAction(Request $request)
    {
        $city = $this->get('app.geoip')->getCity();
        $em    = $this->get('doctrine.orm.entity_manager');
        $query = $em->createQuery("SELECT u FROM AppBundle:User u WHERE u.type=:type AND u.published=true AND u.city=:city ORDER BY u.id DESC");
        $query->setParameter('type', User::PROFILE_TYPE_STO);
        $query->setParameter('city', $city);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)
        );

        return $this->render('front/all_sto.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/for-sto", name="for_sto")
     */
    public function testAction(Request $request)
    {
        $form = $this->createForm(StoType::class, []);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $data['date'] = new \DateTime;

//            $emails = ['zhernosek.maxim@gmail.com'];
            $emails = ['zhernosek.maxim@gmail.com', 'aleksey.paschenko@remoto.by', 'paschenko.aleks@gmail.com', 'buls5800@yandex.by'];

            $message = \Swift_Message::newInstance()
                ->setSubject('Новая заявка от ' . $data['title'])
                ->setFrom('robot@remoto.by')
                ->setTo($emails)
                ->setBody(
                    $this->renderView(
                        'email/new_sto.html.twig',
                        $data
                    ),
                    'text/html'
                )
            ;

            $this->get('mailer')->send($message);

            return $this->redirectToRoute('sto_success');
        }

        return $this->render('front/sto.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/upload", name="upload")
     */
    public function uploadAction(Request $request)
    {
        $path = $this->getParameter('kernel.root_dir') .'/../web/uploads/tmp';

        $files = $request->files->get('files');
        $filesAr = [];

        /**
         * @var $file UploadedFile
         */
        foreach ($files as $file) {
            $name = uniqid('file_') . '.' .$file->getClientOriginalExtension();
            $file->move($path, $name);
            $filesAr[] = $name;
        }

        return new JsonResponse(['files' => $filesAr]);
    }

    /**
     * @Route("/change-city/{id}", name="change_city")
     * @ParamConverter("city", class="AppBundle:City")
     */
    public function changeCityAction(City $city, Request $request)
    {
        $request->getSession()->set('cityByIp', $city->getId());
        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }

    /**
     * @Route("/blog", name="blog")
     */
    public function blogAction()
    {
        $articles = $this->getDoctrine()->getRepository('AppBundle:Article')->findBy([
            'published' => true
        ],[
            'date_created' => 'desc'
        ]);

        return $this->render('front/blog/index.html.twig', ['articles' => $articles]);
    }

    /**
     * @Route("/blog/{id}", name="blog_page", requirements={"id":"\d+"})
     * @ParamConverter("article", class="AppBundle:Article")
     */
    public function articleAction(Article $article)
    {
        return $this->render('front/blog/page.html.twig', ['article' => $article]);
    }
}
