<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Order;
use AppBundle\Entity\Payment\Block;
use AppBundle\Entity\Payment\Debit;
use AppBundle\Entity\Region;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class OrderController extends Controller
{

    /**
     * @Route("/profile/orders", name="profile_orders")
     */
    public function ordersAction()
    {
        $orders = [];
        $user = $this->getUser();

        if ($user->getType() == User::PROFILE_TYPE_USER) {
            $orders = $this->getDoctrine()->getRepository('AppBundle:Order')->findBy([
                'owner' => $this->getUser(),
                'status' => Order::STATUS_NEW
            ], [
                'date_created' => 'desc'
            ]);

            foreach ($orders as $order) {
                $query = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(a.id) FROM AppBundle:Answer a WHERE a.order=:order');
                $query->setParameter('order', $order);
                $count = $query->getSingleScalarResult();

                $order->answersCount = $count;
            }
        } else {
            $qb = $this->getDoctrine()->getEntityManager()->createQueryBuilder();

            $qb
                ->select('o', 'a')
                ->from('AppBundle:Order', 'o')
                ->where('o.status=:status')
                ->join('o.answers', 'a', 'WITH', 'a.owner = :sto')
                ->orderBy('o.date_created', 'desc')
                ->setParameter('status', Order::STATUS_NEW)
                ->setParameter('sto', $user)
            ;

            $orders = $qb->getQuery()->execute();
        }


        return $this->render('profile/orders.html.twig', [
            'user' => $this->getUser(),
            'orders' => $orders
        ]);
    }

    /**
     * @Route("/profile/orders/active", name="profile_orders_active")
     */
    public function ordersActiveAction()
    {
        $orders = [];
        $user = $this->getUser();

        if ($user->getType() == User::PROFILE_TYPE_USER) {
            $orders = $this->getDoctrine()->getRepository('AppBundle:Order')->findBy([
                'owner' => $user,
                'status' => Order::STATUS_IN_PROGRESS
            ], [
                'date_created' => 'desc'
            ]);
        } else {
            $orders = $this->getDoctrine()->getRepository('AppBundle:Order')->findBy([
                'executor' => $user,
                'status' => Order::STATUS_IN_PROGRESS
            ], [
                'date_created' => 'desc'
            ]);
        }

        return $this->render('profile/order/active.html.twig', [
            'orders' => $orders
        ]);
    }

    /**
     * @Route("/profile/orders/archive", name="profile_orders_archive")
     */
    public function ordersArchiveAction()
    {
        $orders = [];
        $user = $this->getUser();

        if ($user->getType() == User::PROFILE_TYPE_USER) {
            $orders = $this->getDoctrine()->getRepository('AppBundle:Order')->findBy([
                'owner' => $user,
                'status' => Order::STATUS_DONE
            ], [
                'date_created' => 'desc'
            ]);
        } else {
            $orders = $this->getDoctrine()->getRepository('AppBundle:Order')->findBy([
                'executor' => $user,
                'status' => Order::STATUS_DONE
            ], [
                'date_created' => 'desc'
            ]);
        }

        return $this->render('profile/order/archive.html.twig', [
            'orders' => $orders
        ]);
    }



    /**
     * @Route("/profile/orders/{id}", name="profile_order_page", requirements={"id": "\d+"})
     * @ParamConverter("order", class="AppBundle:Order")
     * @Security("is_granted('view', order)")
     */
    public function pageAction(Order $order)
    {
        $answer = null;
        $answers = [];
        $answersCount = 0;

        if ($this->getUser()->getType() == User::PROFILE_TYPE_STO) {
            $answer = $this->getDoctrine()->getRepository('AppBundle:Answer')->findOneBy([
                'order' => $order,
                'owner' => $this->getUser()
            ]);
        } else {
            $answers = $this->getDoctrine()->getRepository('AppBundle:Answer')->findBy([
                'order' => $order
                ], ['date_created' => 'asc']);
            $answersCount = count($answers);
        }

        return $this->render('profile/order/page.html.twig', [
            'order' => $order,
            'answer' => $answer,
            'answers' => $answers,
            'answersCount' => $answersCount
        ]);
    }

    /**
     * @Route("/profile/orders/{id}/edit", name="profile_order_edit", requirements={"id": "\d+"})
     * @ParamConverter("order", class="AppBundle:Order")
     * @Security("is_granted('edit', order)")
     */
    public function editAction(Order $order, Request $request)
    {
        $marks = $this->getDoctrine()->getRepository('AppBundle:CarMark')->findBy([], ['title' => 'asc']);

        if ($request->isMethod('POST')) {
            $tmpPath = $this->getParameter('kernel.root_dir') .'/../web/uploads/tmp';
            $path = $this->getParameter('kernel.root_dir') .'/../web/uploads/orders/' . $order->getId() .'/';
            $data = $request->request->all();
            $mark = $this->getDoctrine()->getRepository('AppBundle:CarMark')->find($data['mark']);
            $model = $this->getDoctrine()->getRepository('AppBundle:CarModel')->find($data['model']);
            $em = $this->getDoctrine()->getManager();


            $order
                ->setMark($mark)
                ->setModel($model)
                ->setYear($data['year'])
                ->setText($data['text'])
                ->setSpares(isset($data['spares']) ? true : false)
                ->setTitle($data['title'])
            ;

            if (!empty($data['date'])) {
                $order->setDate(new \DateTime($data['date']));
            } else {
                $order->setDate(null);
            }

            $newImages = [];
            if (isset($data['images'])) {
                $newImages = array_diff($data['images'], $order->getImages());
            }


            foreach ($order->getImages() as $image) {
                if (!in_array($image, $data['images'])) {
                    unlink($image);
                }
            }

            $imagesAr = [];
            if (isset($data['images'])) {
                foreach ($data['images'] as $image) {
                    $imagesAr[] = basename($image);
                }
            }

            $order->setImages(json_encode($imagesAr));

            foreach ($newImages as $file) {
                if ($file) {
                    rename($tmpPath . '/' . $file, $path . $file);
                }
            }

            $em->flush();

            return $this->redirectToRoute('profile_order_page', ['id' => $order->getId()]);

        }

        return $this->render('profile/order/edit.html.twig', [
            'order' => $order,
            'marks' => $marks
        ]);
    }

    /**
     * @Route("/profile/orders/{id}/delete", name="profile_order_delete", requirements={"id": "\d+"})
     * @ParamConverter("order", class="AppBundle:Order")
     * @Security("is_granted('delete', order)")
     */
    public function deleteAction(Order $order)
    {
        $em = $this->getDoctrine()->getManager();
        $path = $this->getParameter('kernel.root_dir') .'/../web';

        foreach ($order->getImages() as $image) {
            if (is_file($path . $image)) {
                unlink($path . $image);
            }
        }

        rmdir($this->getParameter('kernel.root_dir') .'/../web/uploads/orders/' . $order->getId() .'/');

        $em->remove($order);
        $em->flush();

        return $this->redirectToRoute('profile_orders');
    }

    /**
     * @Route("/profile/new-order", name="new_order")
     */
    public function createOrderAction(Request $request)
    {
        if ($this->getUser()->getType() != User::PROFILE_TYPE_USER) {
            return $this->redirectToRoute('profile_orders_search');
        }

        $marks = $this->getDoctrine()->getRepository('AppBundle:CarMark')->findBy([], ['title' => 'asc']);
        $markId = $request->query->get('mark', null);
        $modelId = $request->query->get('model', null);
        $year = $request->query->get('year', null);
        $text = $request->query->get('text', null);

        if ($request->isMethod('POST')) {
            $tmpPath = $this->getParameter('kernel.root_dir') .'/../web/uploads/tmp';
            $path = $this->getParameter('kernel.root_dir') .'/../web/uploads/orders';
            $data = $request->request->all();
            $mark = $this->getDoctrine()->getRepository('AppBundle:CarMark')->find($data['mark']);
            $model = $this->getDoctrine()->getRepository('AppBundle:CarModel')->find($data['model']);
            $em = $this->getDoctrine()->getManager();

            $order = new Order();
            $order
                ->setOwner($this->getUser())
                ->setMark($mark)
                ->setModel($model)
                ->setYear($data['year'])
                ->setText($data['text'])
                ->setSpares(isset($data['spares']) ? true : false)
                ->setDateCreated(new \DateTime())
                ->setTitle($data['title'])
                ->setCity($this->getUser()->getCity())
                ->setType(Order::TYPE_PAID)
            ;

            if (!empty($data['images'])) {
                $order->setImages(json_encode($data['images']));
            }

            if (!empty($data['date'])) {
                $order->setDate(new \DateTime($data['date']));
            }

            $em->persist($order);
            $em->flush();

            mkdir($path . '/' . $order->getId());

            if (!empty($data['images'])) {
                foreach ($data['images'] as $file) {
                    if ($file) {
                        rename($tmpPath . '/' . $file, $path . '/' . $order->getId() . '/' . $file);
                    }
                }
            }


            return $this->redirectToRoute('profile_order_page', ['id' => $order->getId()]);
        }

        return $this->render('profile/new-order.html.twig', [
            'marks' => $marks,
            'markId' => $markId,
            'modelId' => $modelId,
            'year' => $year,
            'text' => $text
        ]);
    }

    /**
     * @Route("/profile/answers/{id}/accept", name="answer_accept")
     * @ParamConverter("answer", class="AppBundle:Answer")
     */
    public function acceptAnswerAction (Answer $answer, Request $request)
    {
        $sto = $answer->getOwner();
        $order = $answer->getOrder();
        $em = $this->getDoctrine()->getManager();

        $order
            ->setStatus(Order::STATUS_IN_PROGRESS)
            ->setExecutor($sto)
            ->setDateBegin(new \DateTime())
        ;

        if ($order->getType() === Order::TYPE_PAID) {
            $debit = new Debit();
            $debit
                ->setOrder($order)
                ->setDateCreated(new \DateTime())
                ->setTotal(Order::PRICE)
                ->setUser($sto)
                ->setText('Заявка №' . $order->getId());

            $sto->setBalance($sto->getBalance() - Order::PRICE);

            $em->persist($debit);
        }

        $em->flush();

        if ($this->get('kernel')->getEnvironment() != 'dev') {
            $this->get('app.mailer')->sendAnswerAcceptedEmail($order);
            $this->get('app.service.sms')->sendAnswerAcceptedSms($order);
        }

        return $this->redirectToRoute('profile_orders_active');
    }

    /**
     * @Route("/profile/orders/{id}/finish", name="profile_finish_order")
     * @ParamConverter("order", class="AppBundle:Order")
     */
    public function finishOrderAction (Order $order)
    {
        $order->setStatus(Order::STATUS_DONE);
        $order->setDateFinish(new \DateTime());

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('profile_orders_archive');
    }

}
