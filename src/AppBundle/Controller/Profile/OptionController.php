<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class OptionController extends Controller
{

    /**
     * @Route("/profile/options", name="profile_options")
     */
    public function indexAction()
    {
        return $this->render('profile/option/index.html.twig', [
            'user' => $this->getUser()
        ]);
    }


}
