<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\Order;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class ProfileController extends Controller
{

    /**
     * @Route("/profile", name="profile")
     */
    public function indexAction()
    {
        if ($this->getUser()->getType() == 1) {
            return $this->redirectToRoute('profile_orders');
        }

        return $this->redirectToRoute('profile_orders_search');
    }



    /**
     * @Route("/profile/{id}", name="profile_page", requirements={"id": "\d+"})
     * @ParamConverter("id", class="AppBundle:User")
     */
    public function profilePageAction(User $user)
    {
        return $this->render('profile/page.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/profile/{id}/portfolio", name="profile_portfolio")
     * @ParamConverter("id", class="AppBundle:User")
     */
    public function portfolioAction(User $user)
    {
        return $this->render('profile/portfolio.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/profile/{id}/contacts", name="profile_contacts")
     * @ParamConverter("id", class="AppBundle:User")
     */
    public function contactsAction(User $user)
    {
        return $this->render('profile/contacts.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/profile/{id}/reviews", name="profile_reviews")
     * @ParamConverter("id", class="AppBundle:User")
     */
    public function reviewsAction(User $user)
    {
        return $this->render('profile/reviews.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/profile/{id}/negative-reviews", name="profile_negative_reviews")
     * @ParamConverter("id", class="AppBundle:User")
     */
    public function negativeReviewsAction(User $user)
    {
        return $this->render('profile/negative_reviews.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/profile/{id}/rating", name="profile_rating")
     * @ParamConverter("id", class="AppBundle:User")
     */
    public function ratingAction(User $user)
    {
        return $this->render('profile/rating.html.twig', [
            'user' => $user
        ]);
    }



    /**
     * @Route("/profile/edit", name="profile_edit")
     */
    public function editAction()
    {
        return $this->render('profile/edit.html.twig', [
            'user' => $this->getUser()
        ]);
    }



    /**
     * @Route("/profile/orders/add", name="profile_orders_add")
     */
    public function addOrderAction()
    {
        return $this->render('profile/order/add.html.twig', [
            'user' => $this->getUser()
        ]);
    }

    public function confirmedAction(Request $request)
    {
        $session = $this->get('session');

        if ($session->has('index_order_mark')) {
            $url = $this->generateUrl('new_order', [
                'mark' => $session->get('index_order_mark', null),
                'model' => $session->get('index_order_model', null),
                'year' => $session->get('index_order_year', null),
                'text' => $session->get('index_order_text', null)
            ]);

            $session->remove('index_order_model');
            $session->remove('index_order_mark');
            $session->remove('index_order_year');
            $session->remove('index_order_text');

            return $this->redirect($url);

        }

        return $this->render('profile/confirmed.html.twig', [
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/profile/email-confirmed", name="profile_email_confirmed")
     */
    public function confirmedEmailAction()
    {
        return $this->render('profile/email_confirmed.html.twig');
    }





}
