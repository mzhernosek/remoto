<?php

namespace AppBundle\Controller\Profile;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Order;
use AppBundle\Entity\Payment\Block;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class StoController extends Controller
{
    /**
     * @Route("/profile/billing", name="profile_billing")
     */
    public function indexAction()
    {
        $payments = $this->getDoctrine()->getRepository('AppBundle:Payment\Payment')->findBy([
            'user' => $this->getUser()
        ], ['date_created' => 'desc']);

        return $this->render('profile/billing/index.html.twig', [
            'payments' => $payments
        ]);
    }

    /**
     * @Route("/profile/billing/blocks", name="profile_billing_blocks")
     */
    public function blocksAction()
    {
        $blocks = $this->getDoctrine()->getRepository('AppBundle:Payment\Block')->findBy([
            'user' => $this->getUser(),
            'enabled' => true
        ], ['date_created' => 'desc']);

        return $this->render('profile/billing/blocks.html.twig', [
            'blocks' => $blocks
        ]);
    }

    /**
     * @Route("/profile/billing/debits", name="profile_billing_debits")
     */
    public function debitsAction()
    {
        $debits = $this->getDoctrine()->getRepository('AppBundle:Payment\Debit')->findBy([
            'user' => $this->getUser()
        ], ['date_created' => 'desc']);

        return $this->render('profile/billing/debits.html.twig', [
            'debits' => $debits
        ]);
    }

    /**
     * @Route("/profile/orders/search", name="profile_orders_search")
     */
    public function searchAction()
    {
        $orders = $this->getDoctrine()->getRepository('AppBundle:Order')->findBy([
            'status' => Order::STATUS_NEW,
            'city' => $this->getUser()->getCity()
        ], [
            'date_created' => 'desc'
        ]);
        $ordersAr = [];

        foreach ($orders as $order) {
            $answer = $this->getDoctrine()->getRepository('AppBundle:Answer')->findOneBy([
                'order' => $order,
                'owner' => $this->getUser()
            ]);

            if (is_null($answer)) {
                $ordersAr[] = $order;
            }
        }

        return $this->render('profile/order/search.html.twig', [
            'user' => $this->getUser(),
            'orders' => $ordersAr
        ]);
    }

    /**
     * @Method("POST")
     * @Route("/profile/orders/{id}/answers", name="answers_make")
     * @ParamConverter("order", class="AppBundle:Order")
     */
    public function makeAnswerAction (Order $order, Request $request)
    {
        $answer = new Answer();
        $answer
            ->setOrder($order)
            ->setDateCreated(new \DateTime())
            ->setOwner($this->getUser())
            ->setPrice((int)$request->request->get('price'))
            ->setComment($request->request->get('comment', null));

        $em = $this->getDoctrine()->getManager();
        $em->persist($answer);
        $em->flush();

        if ($this->get('kernel')->getEnvironment() != 'dev') {
            $this->get('app.mailer')->sendNewOfferEmail($order);
            $this->get('app.service.sms')->sendNewOfferSms($order);
        }

        return $this->redirectToRoute('profile_order_page', ['id' => $order->getId()]);
    }

    /**
     * @Method("GET")
     * @Route("/profile/answers/{id}", name="answers_delete")
     * @ParamConverter("answer", class="AppBundle:Answer")
     * @Security("is_granted('delete', answer)")
     */
    public function deleteAnswerAction(Answer $answer)
    {
        $url = $this->generateUrl('profile_order_page', ['id' => $answer->getOrder()->getId()]);

        $em = $this->getDoctrine()->getManager();
        $em->remove($answer);
        $em->flush();

        return $this->redirect($url);
    }

}