<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{

    /**
     * @Route("/admin", name="admin")
     */
    public function indexAction()
    {
        return $this->render('admin_layout.html.twig');
    }

    /**
     * @Route("/admin/{all}", requirements={"all" = ".+"})

     * @Method({"GET"})
     */
    public function catchRouteAction()
    {
        return $this->render('admin_layout.html.twig');
    }
}
