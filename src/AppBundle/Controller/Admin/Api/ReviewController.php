<?php

namespace AppBundle\Controller\Admin\Api;

use AppBundle\Entity\Category;
use AppBundle\Entity\Input\CreateCategory;
use AppBundle\Entity\Input\CreateReview;
use AppBundle\Entity\Review;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ReviewController extends RestController
{
    /**
     * @Rest\Get("/api/admin/reviews", name="api_admin_get_reviews")
     */
    public function getAllAction()
    {
        return $this->getDoctrine()->getRepository('AppBundle:Review')->findAll();
    }

    /**
     * @Rest\Get("/api/admin/reviews/{id}", name="api_admin_get_review")
     * @ParamConverter("category", class="AppBundle:Review")
     */
    public function getAction(Review $review)
    {
        return $review;
    }

    /**
     * @Rest\Post("/api/admin/reviews", name="api_admin_post_reviews")
     * @ParamConverter("createReview", converter="fos_rest.request_body")
     */
    public function postAction(CreateReview $createReview)
    {
        $review = new Review;
        $review->setName($createReview->name);
        $review->setMark($createReview->mark);
        $review->setText($createReview->text);
        $em = $this->getDoctrine()->getManager();
        $em->persist($review);
        $em->flush();

        return $review;
    }

    /**
     * @Rest\Put("/api/admin/reviews/{id}", name="api_admin_put_reviews")
     * @ParamConverter("createReview", converter="fos_rest.request_body")
     * @ParamConverter("review", class="AppBundle:Review")
     */
    public function putAction(CreateReview $createReview, Review $review)
    {
        $review->setName($createReview->name);
        $review->setMark($createReview->mark);
        $review->setText($createReview->text);

        $this->getDoctrine()->getManager()->flush();

        return $review;
    }

    /**
     * @Rest\Delete("/api/admin/reviews/{id}", name="api_admin_delete_reviews")
     * @ParamConverter("review", class="AppBundle:Review")
     */
    public function deleteAction(Review $review)
    {
        $uploaddir = $this->get('kernel')->getRootDir().'/../web/uploads/reviews/';
        if ($review->getImage()) {
            unlink($uploaddir.$review->getImage());
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($review);
        $em->flush();
    }

    /**
     * @Rest\Post("/api/admin/reviews/{id}/image", name="api_admin_post_reviews_image")
     * @ParamConverter("review", class="AppBundle:Review")
     */
    public function imageAction(Review $review)
    {
        $uploaddir = $this->get('kernel')->getRootDir().'/../web/uploads/reviews/';

        $ext = explode('.', basename($_FILES['file']['name']))[1];
        $file = 'file_'.uniqid().'.'.$ext;
        $uploadfile = $uploaddir . $file;

        if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {

            if ($review->getImage()) {
                unlink($uploaddir.$review->getImage());
            }

            $review->setImage($file);
            $this->getDoctrine()->getManager()->flush();
        } else {

        }

        return [];
    }
}
