<?php

namespace AppBundle\Controller\Admin\Api;

use AppBundle\Entity\Category;
use AppBundle\Entity\Input\CreateCategory;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class CategoryController extends RestController
{
    /**
     * @Rest\Get("/api/admin/categories", name="api_admin_get_categories")
     */
    public function getAllAction()
    {
        return $this->getDoctrine()->getRepository('AppBundle:Category')->findBy(['parent' => 0]);
    }

    /**
     * @Rest\Get("/api/admin/categories/{id}/subcategories", name="api_admin_get_subcategories")
     * @ParamConverter("category", class="AppBundle:Category")
     */
    public function getSubcategoriesAction(Category $category)
    {
        return $this->getDoctrine()->getRepository('AppBundle:Category')->findBy([
            'parent' => $category->getId()
        ]);
    }

    /**
     * @Rest\Get("/api/admin/categories/{id}", name="api_admin_get_category")
     * @ParamConverter("category", class="AppBundle:Category")
     */
    public function getAction(Category $category)
    {
        return $category;
    }

    /**
     * @Rest\Post("/api/admin/categories", name="api_admin_post_categories")
     * @ParamConverter("createCategory", converter="fos_rest.request_body")
     */
    public function postAction(CreateCategory $createCategory)
    {
        $category = new Category;
        $category->setTitle($createCategory->title);

        if ($createCategory->parent) {
            $category->setParent($createCategory->parent);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();

        return $category;
    }

    /**
     * @Rest\Put("/api/admin/categories/{id}", name="api_admin_put_categories")
     * @ParamConverter("createCategory", converter="fos_rest.request_body")
     * @ParamConverter("category", class="AppBundle:Category")
     */
    public function putAction(CreateCategory $createCategory, Category $category)
    {
        $category->setTitle($createCategory->title);
        $this->getDoctrine()->getManager()->flush();

        return $category;
    }

    /**
     * @Rest\Delete("/api/admin/categories/{id}", name="api_admin_delete_categories")
     * @ParamConverter("category", class="AppBundle:Category")
     */
    public function deleteAction(Category $category)
    {
        $uploaddir = $this->get('kernel')->getRootDir().'/../web/uploads/services/';
        if ($category->getImage()) {
            unlink($uploaddir.$category->getImage());
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();
    }

    /**
     * @Rest\Post("/api/admin/categories/{id}/image", name="api_admin_post_categories_image")
     * @ParamConverter("category", class="AppBundle:Category")
     */
    public function imageAction(Category $category)
    {
        $uploaddir = $this->get('kernel')->getRootDir().'/../web/uploads/services/';

        $ext = explode('.', basename($_FILES['file']['name']))[1];
        $file = 'file_'.uniqid().'.'.$ext;
        $uploadfile = $uploaddir . $file;

        move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);

            if ($category->getImage()) {
                unlink($uploaddir.$category->getImage());
            }

            $category->setImage($file);
            $this->getDoctrine()->getManager()->flush();


        return [];
    }


}
