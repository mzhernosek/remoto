<?php

namespace AppBundle\Controller\Admin\Api;

use AppBundle\Entity\Input\CreateSeo;
use AppBundle\Entity\Seo;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class SeoController extends RestController
{
    /**
     * @Rest\Get("/api/admin/seo/all", name="api_admin_get_all_seo")
     */
    public function getAllAction()
    {
        return $this->getDoctrine()->getRepository('AppBundle:Seo')->findAll();
    }

    /**
     * @Rest\Get("/api/admin/seo/{id}", name="api_admin_get_seo")
     * @ParamConverter("option", class="AppBundle:Option")
     */
    public function getAction(Seo $seo)
    {
        return $seo;
    }

    /**
     * @Rest\Put("/api/admin/seo/{id}", name="api_admin_put_seo")
     * @ParamConverter("createSeo", converter="fos_rest.request_body")
     * @ParamConverter("seo", class="AppBundle:Seo")
     */
    public function putAction(Seo $seo, CreateSeo $createSeo)
    {
        $seo
            ->setTitle($createSeo->title)
            ->setKeywords($createSeo->keywords)
            ->setDescription($createSeo->description)
            ->setText($createSeo->text);

        $this->getDoctrine()->getManager()->flush();

        return $seo;
    }



}
