<?php

namespace AppBundle\Controller\Admin\Api;

use AppBundle\Entity\CarMark;
use AppBundle\Entity\CarModel;
use AppBundle\Entity\City;
use AppBundle\Entity\Input\CreateCar;
use AppBundle\Entity\Input\CreatePayment;
use AppBundle\Entity\Input\CreateRegion;
use AppBundle\Entity\Order;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Region;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class AnswerController extends RestController
{
    /**
     * @Rest\Get("/api/admin/orders/{id}/answers")
     * @ParamConverter("order", class="AppBundle:Order")
     */
    public function getAnswersAction(Order $order)
    {
        $answers = $this->getDoctrine()->getRepository('AppBundle:Answer')->findBy([
            'order' => $order
        ], ['date_created' => 'asc']);

        return $answers;
    }
}