<?php

namespace AppBundle\Controller\Admin\Api;

use AppBundle\Entity\Article;
use AppBundle\Entity\CarMark;
use AppBundle\Entity\CarModel;
use AppBundle\Entity\City;
use AppBundle\Entity\Input\CreateArticle;
use AppBundle\Entity\Input\CreateCar;
use AppBundle\Entity\Input\CreatePayment;
use AppBundle\Entity\Input\CreateRegion;
use AppBundle\Entity\Order;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Region;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends RestController
{
    /**
     * @Rest\Post("/api/admin/articles")
     * @ParamConverter("createArticle", converter="fos_rest.request_body")
     */
    public function postAction(CreateArticle $createArticle)
    {
        $article = new Article();
        $article->setTitle($createArticle->title);
        $article->setDateCreated(new \DateTime());

        $em = $this->get('doctrine.orm.entity_manager');
        $em->persist($article);
        $em->flush();

        return $article;
    }

    /**
     * @Rest\Get("/api/admin/articles")
     */
    public function getAllAction(Request $request)
    {
        $page = $request->query->get('page', 0);
        $count = $request->query->get('count');

        $data = $this->getDoctrine()->getRepository('AppBundle:Article')->findBy(
            [],
            [
                'date_created' => 'desc'
            ],
            $count,
            ($page - 1) * $count
        );

        $query = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(a.id) FROM AppBundle:Article a');
        $count = $query->getSingleScalarResult();

        return [
            'count' => $count,
            'data' => $data
        ];
    }

    /**
     * @Rest\Get("/api/admin/articles/{id}")
     * @ParamConverter("article", class="AppBundle:Article")
     */
    public function getAction(Article $article)
    {
        return $article;
    }

    /**
     * @Rest\Put("/api/admin/articles/{id}")
     * @ParamConverter("createArticle", converter="fos_rest.request_body")
     * @ParamConverter("article", class="AppBundle:Article")
     */
    public function putAction(CreateArticle $createArticle, Article $article)
    {
        $article->setTitle($createArticle->title);
        $article->setDescription($createArticle->description);
        $article->setText($createArticle->text);
        $article->setPublished($createArticle->published);
        $article->setShortText($createArticle->short_text);

        $this->getDoctrine()->getManager()->flush();

        return $article;
    }

    /**
     * @Rest\Delete("/api/admin/articles/{id}", requirements={"id": "\d+"})
     * @ParamConverter("article", class="AppBundle:Article")
     */
    public function deleteAction(Article $article)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($article);
        $em->flush();
    }

    /**
     * @Rest\Post("/api/admin/articles/{id}/upload/editor-image", requirements={"id": "\d+"})
     * @ParamConverter("article", class="AppBundle:Article")
     */
    public function uploadImageAction(Request $request, Article $article)
    {
        $path = $this->getParameter('kernel.root_dir') .'/../web/uploads/articles/' . $article->getId();
        $image = '';

        if ($request->files->has('file')) {
            $file = $request->files->get('file');

            if (!is_dir($path)) {
                mkdir($path);
            }

            $name = uniqid('image_') . '.' .$file->getClientOriginalExtension();
            $file->move($path, $name);
            $image = $name;
        }

        return new JsonResponse([
            'image' => $image
        ]);
    }
}