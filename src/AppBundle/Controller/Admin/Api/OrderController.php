<?php

namespace AppBundle\Controller\Admin\Api;

use AppBundle\Entity\City;
use AppBundle\Entity\Input\CreateOrder;
use AppBundle\Entity\Input\CreateRegion;
use AppBundle\Entity\Order;
use AppBundle\Entity\Region;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class OrderController extends RestController
{
    /**
     * @Rest\Get("/api/admin/orders")
     */
    public function getAllAction(Request $request)
    {
        $page = $request->query->get('page', 0);
        $count = $request->query->get('count');

        $data = $this->getDoctrine()->getRepository('AppBundle:Order')->findBy(
            [],
            [
                'date_created' => 'desc'
            ],
            $count,
            ($page - 1) * $count
        );


        foreach ($data as $order) {
            $query = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(a.id) FROM AppBundle:Answer a WHERE a.order=:order');
            $query->setParameter('order', $order);
            $count = $query->getSingleScalarResult();

            $order->answersCount = $count;
        }


        $query = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(o.id) FROM AppBundle:Order o');
        $count = $query->getSingleScalarResult();

        return [
            'count' => $count,
            'data' => $data
        ];
    }

    /**
     * @Rest\Get("/api/admin/orders/{id}", requirements={"id": "\d+"})
     * @ParamConverter("order", class="AppBundle:Order")
     */
    public function getAction(Order $order)
    {
        return $order;
    }

    /**
     * @Rest\Put("/api/admin/orders/{id}", requirements={"id": "\d+"})
     * @ParamConverter("createOrder", converter="fos_rest.request_body")
     * @ParamConverter("order", class="AppBundle:Order")
     */
    public function putAction(Order $order, CreateOrder $createOrder)
    {
        $order->setTitle($createOrder->title);
        $order->setText($createOrder->text);
        $order->setType($createOrder->type);
        $this->getDoctrine()->getManager()->flush();

        return $order;
    }

}