<?php

namespace AppBundle\Controller\Admin\Api;

use AppBundle\Entity\CarMark;
use AppBundle\Entity\CarModel;
use AppBundle\Entity\City;
use AppBundle\Entity\Input\CreateCar;
use AppBundle\Entity\Input\CreateRegion;
use AppBundle\Entity\Region;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class CarController extends RestController
{
    /**
     * @Rest\Get("/api/admin/marks/{id}")
     * @ParamConverter("mark", class="AppBundle:CarMark")
     */
    public function getAction(CarMark $mark)
    {
        return $mark;
    }

    /**
     * @Rest\Put("/api/admin/marks/{id}")
     * @ParamConverter("createCar", converter="fos_rest.request_body")
     * @ParamConverter("mark", class="AppBundle:CarMark")
     */
    public function putAction(CreateCar $createCar, CarMark $mark)
    {
        $mark->setTitle($createCar->title);
        $this->getDoctrine()->getManager()->flush();

        return $mark;
    }

    /**
     * @Rest\Post("/api/admin/marks")
     * @ParamConverter("createCar", converter="fos_rest.request_body")
     */
    public function postAction(CreateCar $createCar)
    {
        $mark = new CarMark();
        $mark->setTitle($createCar->title);

        $em = $this->getDoctrine()->getManager();
        $em->persist($mark);
        $em->flush();

        return $mark;
    }

    /**
     * @Rest\Delete("/api/admin/marks/{id}")
     * @ParamConverter("mark", class="AppBundle:CarMark")
     */
    public function deleteAction(CarMark $mark)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($mark);
        $em->flush();
    }

    /**
     * @Rest\Post("/api/admin/marks/{id}/models")
     * @ParamConverter("createCar", converter="fos_rest.request_body")
     * @ParamConverter("mark", class="AppBundle:CarMark")
     */
    public function postModelAction(CreateCar $createCar, CarMark $mark)
    {
        $model = new CarModel();
        $model->setTitle($createCar->title);
        $model->setMark($mark);

        $em = $this->getDoctrine()->getManager();
        $em->persist($model);
        $em->flush();

        return $model;
    }

    /**
     * @Rest\Get("/api/admin/models/{id}")
     * @ParamConverter("model", class="AppBundle:CarModel")
     */
    public function getModelAction(CarModel $model)
    {
        return $model;
    }

    /**
     * @Rest\Delete("/api/admin/models/{id}")
     * @ParamConverter("model", class="AppBundle:CarModel")
     */
    public function deleteModelAction(CarModel $model)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($model);
        $em->flush();
    }

    /**
     * @Rest\Put("/api/admin/models/{id}")
     * @ParamConverter("createCar", converter="fos_rest.request_body")
     * @ParamConverter("model", class="AppBundle:CarModel")
     */
    public function putModelAction(CreateCar $createCar, CarModel $model)
    {
        $model->setTitle($createCar->title);
        $this->getDoctrine()->getManager()->flush();

        return $model;
    }
}