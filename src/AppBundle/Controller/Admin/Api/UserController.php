<?php

namespace AppBundle\Controller\Admin\Api;

use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class UserController extends RestController
{
    /**
     * @Rest\Get("/api/admin/users/all", name="api_admin_get_all_users")
     */
    public function getAllAction(Request $request)
    {
        $page = $request->query->get('page', 0);
        $count = $request->query->get('count');

        $users =  $this->getDoctrine()->getRepository('AppBundle:User')->findBy(
            [
                'type' => User::PROFILE_TYPE_USER
            ],
            [
                'id' => 'asc'
            ],
            $count,
            ($page - 1) * $count
        );

        foreach ($users as $user) {
            $user->switchUrl = $this->generateUrl('profile', ['_switch_user' => $user->getEmail()]);
        }

        $query = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(u.id) FROM AppBundle:User u WHERE u.type=:type');
        $query->setParameter('type', User::PROFILE_TYPE_USER);
        $count = $query->getSingleScalarResult();

        return [
            'count' => $count,
            'data' => $users
        ];
    }
}