<?php

namespace AppBundle\Controller\Admin\Api;

use AppBundle\Entity\CarMark;
use AppBundle\Entity\CarModel;
use AppBundle\Entity\City;
use AppBundle\Entity\Input\CreateCar;
use AppBundle\Entity\Input\CreatePayment;
use AppBundle\Entity\Input\CreateRegion;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Region;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class BillingController extends RestController
{
    /**
     * @Rest\Post("/api/admin/sto/{id}/payments")
     * @ParamConverter("createPayment", converter="fos_rest.request_body")
     * @ParamConverter("sto", class="AppBundle:User")
     */
    public function postPaymentAction(CreatePayment $createPayment, User $sto)
    {
        $payment = new Payment();
        $payment
            ->setText($createPayment->text)
            ->setDateCreated(new \DateTime())
            ->setTotal($createPayment->total)
            ->setUser($sto)
        ;

        $balance = (float)$sto->getBalance();
        $balance += $createPayment->total;
        $sto->setBalance($balance);

        $em = $this->getDoctrine()->getManager();
        $em->persist($payment);
        $em->flush();

        return $payment;
    }

    /**
     * @Rest\Get("/api/admin/sto/{id}/payments")
     * @ParamConverter("sto", class="AppBundle:User")
     */
    public function stoListPaymentsAction (User $sto, Request $request)
    {
        $page = $request->request->get('page', 0);
        $count = $request->request->get('count');

        $data =  $this->getDoctrine()->getRepository('AppBundle:Payment\Payment')->findBy(
            [
                'user' => $sto
            ],
            null,
            $count,
            ($page - 1) * $count
        );

        $query = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(p.id) FROM AppBundle:Payment\Payment p WHERE p.user=:user');
        $query->setParameter('user', $sto);
        $count = $query->getSingleScalarResult();

        return [
            'count' => $count,
            'data' => $data
        ];
    }
}