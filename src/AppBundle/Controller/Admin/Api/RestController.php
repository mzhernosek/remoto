<?php

namespace AppBundle\Controller\Admin\Api;

use AppBundle\Entity\Output\Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;

class RestController extends FOSRestController
{
    protected function handleError($message, $data = null, $code = 400, $status = 400)
    {
        $result = new Rest($code, $message, $data);
        return $this->view($result, $status);
    }

    protected function response($message, $data = null, $status = 400)
    {
        $result = new Rest($status, $message, $data);
        return $this->view($result, $status);
    }


}