<?php

namespace AppBundle\Controller\Admin\Api;

use AppBundle\Entity\Input\CreateSeo;
use AppBundle\Entity\Input\CreateSto;
use AppBundle\Entity\Seo;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class StoController extends RestController
{
    /**
     * @Rest\Post("/api/admin/sto")
     * @ParamConverter("createSto", converter="fos_rest.request_body")
     */
    public function postAction(CreateSto $createSto)
    {

        $region = $this->getDoctrine()->getRepository('AppBundle:Region')->find($createSto->region);
        $city = $this->getDoctrine()->getRepository('AppBundle:City')->find($createSto->city);
        $userManager = $this->get('fos_user.user_manager');

        $user = new User();
        $user
            ->setTitle($createSto->title)
            ->setCity($city)
            ->setRegion($region)
            ->setEnabled(true)
            ->setType(User::PROFILE_TYPE_STO)
            ->setPublished(false)
        ;

        if ($createSto->email) {
            $password = rand(pow(10, 4), pow(10,5) - 1);

            $user
                ->setEmail($createSto->email)
                ->setPlainPassword($password)
                ->setPhone($createSto->phone);

            $orders = $this->getDoctrine()->getRepository('AppBundle:Order')->findBy(
                [],
                ['date_created' => 'desc'],
                3
            );

            $this->get('app.mailer')->sendStoCreatedMessage($user, $password, $orders);
        }

        $userManager->updateUser($user);





        $this->getDoctrine()->getEntityManager()->flush();

        return $user;
    }

    /**
     * @Rest\Get("/api/admin/sto")
     */
    public function listAction(Request $request)
    {
        $page = $request->query->get('page', 0);
        $count = $request->query->get('count');

        $users =  $this->getDoctrine()->getRepository('AppBundle:User')->findBy(
            [
                'type' => User::PROFILE_TYPE_STO
            ],
            [
                'id' => 'asc'
            ],
            $count,
            ($page - 1) * $count
        );

        foreach ($users as $user) {
            $user->switchUrl = $this->generateUrl('profile', ['_switch_user' => $user->getEmail()]);
        }

        $query = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(u.id) FROM AppBundle:User u WHERE u.type=:type');
        $query->setParameter('type', User::PROFILE_TYPE_STO);
        $count = $query->getSingleScalarResult();

        return [
            'count' => $count,
            'data' => $users
        ];
    }

    /**
     * @Rest\Get("/api/admin/sto/{id}")
     * @ParamConverter("sto", class="AppBundle:User")
     */
    public function getAction(User $sto)
    {
        return $sto;
    }

    /**
     * @Rest\Put("/api/admin/sto/{id}")
     * @ParamConverter("sto", class="AppBundle:User")
     * @ParamConverter("createSto", converter="fos_rest.request_body")
     */
    public function putAction(CreateSto $createSto, User $sto)
    {
        $sto
            ->setTitle($createSto->title)
            ->setAddress($createSto->address)
            ->setLat($createSto->lat)
            ->setLon($createSto->lon)
            ->setSlug($createSto->slug)
            ->setDescription($createSto->description)
            ->setPublished($createSto->published)
        ;

        if ($createSto->services) {
            $sto->setServices($createSto->services);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($sto);
        $em->flush();

        return $sto;
    }

    /**
     * @Rest\Post("/api/admin/sto/{id}/upload/image", requirements={"id": "\d+"})
     * @ParamConverter("sto", class="AppBundle:User")
     */
    public function uploadImageAction(Request $request, User $sto)
    {
        $path = $this->getParameter('kernel.root_dir') .'/../web/uploads/sto/' . $sto->getId();
        $image = '';


        if ($request->files->has('file')) {
            $file = $request->files->get('file');

            if (!is_dir($path)) {
                mkdir($path);
            }

            $name = uniqid('image_') . '.' .$file->getClientOriginalExtension();
            $file->move($path, $name);
            $image = $name;

            $sto->setImage($image);
            $this->getDoctrine()->getManager()->flush();
        }



        return new JsonResponse([
            'image' => $image
        ]);
    }

    /**
     * @Rest\Post("/api/admin/sto/{id}/upload/images", requirements={"id": "\d+"})
     * @ParamConverter("sto", class="AppBundle:User")
     */
    public function uploadImagseAction(Request $request, User $sto)
    {
        $path = $this->getParameter('kernel.root_dir') .'/../web/uploads/sto/' . $sto->getId();
        $images = $sto->getImages();


        if ($request->files->has('files')) {

            $files = $request->files->get('files');

            if (!is_dir($path)) {
                mkdir($path);
            }

            foreach ($files as $file) {
                $name = uniqid('images_') . '.' .$file->getClientOriginalExtension();
                $file->move($path, $name);
                $images[] = $name;
            }

            $sto->setImages($images);
            $this->getDoctrine()->getManager()->flush();
        }



        return new JsonResponse([
            'images' => $images
        ]);
    }

    /**
     * @Rest\Post("/api/admin/sto/{id}/upload/editor-image", requirements={"id": "\d+"})
     * @ParamConverter("sto", class="AppBundle:User")
     */
    public function uploadEditorImageAction(Request $request, User $sto)
    {
        $path = $this->getParameter('kernel.root_dir') .'/../web/uploads/sto/' . $sto->getId();
        $image = '';

        if ($request->files->has('file')) {
            $file = $request->files->get('file');

            if (!is_dir($path)) {
                mkdir($path);
            }

            $name = uniqid('eimage_') . '.' .$file->getClientOriginalExtension();
            $file->move($path, $name);
            $image = $name;

            $this->getDoctrine()->getManager()->flush();
        }



        return new JsonResponse([
            'image' => $image
        ]);
    }


}