<?php

namespace AppBundle\Controller\Admin\Api;

use AppBundle\Entity\City;
use AppBundle\Entity\Input\CreateRegion;
use AppBundle\Entity\Region;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class RegionController extends RestController
{
    /**
     * @Rest\Post("/api/admin/regions")
     * @ParamConverter("createRegion", converter="fos_rest.request_body")
     */
    public function postAction(CreateRegion $createRegion)
    {
        $region = new Region;
        $region->setTitle($createRegion->title);

        $em = $this->getDoctrine()->getManager();
        $em->persist($region);
        $em->flush();

        return $region;
    }

    /**
     * @Rest\Get("/api/admin/regions")
     */
    public function getAllAction(Request $request)
    {
        $page = $request->request->get('page', 0);
        $count = $request->request->get('count');

        $data =  $this->getDoctrine()->getRepository('AppBundle:Region')->findBy(
            [],
            null,
            $count,
            ($page - 1) * $count
        );

        $query = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(u.id) FROM AppBundle:Region u');
        $count = $query->getSingleScalarResult();

        return [
            'count' => $count,
            'data' => $data
        ];
    }

    /**
     * @Rest\Get("/api/admin/regions/{id}/cities")
     * @ParamConverter("region", class="AppBundle:Region")
     */
    public function getAllCitiesAction(Request $request, Region $region)
    {
        $page = $request->request->get('page', 0);
        $count = $request->request->get('count');

        $data =  $this->getDoctrine()->getRepository('AppBundle:City')->findBy(
            [
                'region' => $region
            ],
            null,
            $count,
            ($page - 1) * $count
        );

        $query = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(c.id) FROM AppBundle:City c WHERE c.region=:region');
        $query->setParameter('region', $region);
        $count = $query->getSingleScalarResult();

        return [
            'count' => $count,
            'data' => $data
        ];
    }

    /**
     * @Rest\Post("/api/admin/regions/{id}/cities")
     * @ParamConverter("createCity", converter="fos_rest.request_body")
     * @ParamConverter("region", class="AppBundle:Region")
     */
    public function postCityAction(CreateRegion $createCity, Region $region)
    {
        $city = new City;
        $city->setTitle($createCity->title);
        $city->setRegion($region);

        $em = $this->getDoctrine()->getManager();
        $em->persist($city);
        $em->flush();

        return $city;
    }

}