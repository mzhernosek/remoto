<?php

namespace AppBundle\Controller\Admin\Api;

use AppBundle\Entity\Input\CreateOption;
use AppBundle\Entity\Option;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class OptionController extends RestController
{
    /**
     * @Rest\Get("/api/admin/options", name="api_admin_get_options")
     */
    public function getAction()
    {
        $options = $this->getDoctrine()->getRepository('AppBundle:Option')->findAll();

        return $options;
    }

    /**
     * @Rest\Put("/api/admin/options/{id}", name="api_admin_update_options", requirements={"id" = "\d+"})
     * @ParamConverter("createOption", converter="fos_rest.request_body")
     * @ParamConverter("option", class="AppBundle:Option")
     */
    public function updateAction(CreateOption $createOption, Option $option)
    {
        $option->setValue($createOption->value);
        $this->getDoctrine()->getManager()->flush();

        return $option;
    }

    /**
     * @Rest\Get("/api/admin/options/{key}", name="api_admin_options_get_by_key", requirements={"id" = "\w+"})
     */
    public function getByKeyAction(Request $request)
    {
        $key = $request->attributes->get('key');

        return $this->getDoctrine()->getRepository('AppBundle:Option')->findOneBy(['key' => $key]);
    }

    /**
     * @Rest\Post("/api/admin/options/image", name="api_admin_post_options_image")
     */
    public function imageAction()
    {
        $uploaddir = $this->get('kernel')->getRootDir().'/../web/uploads/slider/';

        $ext = explode('.', basename($_FILES['file']['name']))[1];
        $file = 'file_'.uniqid().'.'.$ext;
        $uploadfile = $uploaddir . $file;

        move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);

        return [
            'file' => $file
        ];
    }

    /**
     * @Rest\Put("/api/admin/options/image/{file}", name="api_admin_put_options_image")
     */
    public function imageRemoveAction(Request $request)
    {
        $uploaddir = $this->get('kernel')->getRootDir().'/../web/uploads/slider/';
        $file = $uploaddir.$request->attributes->get('file');

        unlink($file);

        return [];
    }


}
