<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CarMark;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class AjaxController extends Controller
{
    /**
     * @Route("/ajax/check-email", name="ajax_check_email")
     */
    public function indexAction(Request $request)
    {
        $email = $request->request->get('email');

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $checkUser = $this->getDoctrine()->getManager()->getRepository('AppBundle:User')->findOneBy(['email' => $email]);

            if ($checkUser) {
                return new JsonResponse(['status' => false, 'error' => 2]);
            } else {
                return new JsonResponse(['status' => true]);
            }
        } else {
            return new JsonResponse(['status' => false, 'error' => 1]);
        }
    }

    /**
     * @Route("/ajax/sms/send", name="ajax_sms_send", options={"expose"=true})
     */
    public function sendSmsConfirmAction(Request $request)
    {
        $phones = [
//            '+375298139395',
//            '+375333469117',
//            '+375292176379'
        ];
        $response = [];
        $phone = $request->request->get('phone');
        $email = $request->request->get('email');

        $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(['email' => $email]);
        if ($user) {
            return new JsonResponse(['status' => false, 'email' => true]);
        }

        $code = $this->get('app.service.sms')->sendSmsConfirm($phone);

        if (in_array($phone, $phones)) {
            $response['code'] = $code;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/ajax/sms/confirm", name="ajax_sms_confirm", options={"expose"=true})
     */
    public function confirmSmsAction (Request $request)
    {
        $code = $request->request->get('code');
        $result = $this->get('app.service.sms')->checkSmsConfirm($code);

        return new JsonResponse(['status' => $result]);
    }

    /**
     * @Route("/ajax/marks")
     */
    public function getMarks (Request $request)
    {
        $page = $request->query->get('page', 0);
        $count = $request->query->get('count');


        $data =  $this->getDoctrine()->getRepository('AppBundle:CarMark')->findBy(
            [],
            [
                'title' => 'asc'
            ],
            $count,
            ($page - 1) * $count
        );
        $responseData = [];

        foreach ($data as $mark) {
            $responseData[] = [
                'id' => $mark->getId(),
                'title' => $mark->getTitle()
            ];
        }

        $query = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(c.id) FROM AppBundle:CarMark c');
        $count = $query->getSingleScalarResult();

        return new JsonResponse([
            'count' => $count,
            'data' => $responseData
        ]);
    }

    /**
     * @Route("/ajax/marks/{id}/models", name="ajax_get_models", options={"expose"=true})
     * @ParamConverter("mark", class="AppBundle:CarMark")
     */
    public function getModelsAction (Request $request, CarMark $mark)
    {
        $page = $request->query->get('page', 0);
        $count = $request->query->get('count');


        $data =  $this->getDoctrine()->getRepository('AppBundle:CarModel')->findBy(
            [
                'mark' => $mark
            ],
            [
                'title' => 'asc'
            ],
            $count,
            ($page - 1) * $count
        );
        $responseData = [];

        foreach ($data as $model) {
            $responseData[] = [
                'id' => $model->getId(),
                'title' => $model->getTitle()
            ];
        }

        $query = $this->getDoctrine()->getManager()->createQuery('SELECT COUNT(c.id) FROM AppBundle:CarModel c WHERE c.mark=:mark');
        $query->setParameter('mark', $mark);
        $count = $query->getSingleScalarResult();

        return new JsonResponse([
            'count' => $count,
            'data' => $responseData
        ]);
    }

    /**
     * @Route("/ajax/order/save", name="ajax_save_data", options={"expose"=true})
     */
    public function saveOrderAction (Request $request)
    {
        $mark = $request->request->get('mark');
        $model = $request->request->get('model');
        $year = $request->request->get('year');
        $text = $request->request->get('text');

        $session = $this->get('session');
        $session->set('index_order_mark', $mark);
        $session->set('index_order_model', $model);
        $session->set('index_order_text', $text);
        $session->set('index_order_year', $year);


        return new JsonResponse([

        ]);
    }
}
