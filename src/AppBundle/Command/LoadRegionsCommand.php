<?php

namespace AppBundle\Command;

use AppBundle\Entity\City;
use AppBundle\Entity\Option;
use AppBundle\Entity\Region;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadRegionsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:load:regions')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Begin');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $region = new Region();
        $region->setTitle('Витебская область');
        $em->persist($region);

        $city = new City();
        $city->setTitle('Витебск');
        $city->setRegion($region);

        $em->persist($city);


        $em->flush();

        $output->writeln('Finished');
    }
}