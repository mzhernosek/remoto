<?php

namespace AppBundle\Command;

use AppBundle\Entity\Seo;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadSeoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:load:seo')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Begin');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository('AppBundle:Seo');
        $seosAr = [
            [
                'title' => 'Главная страница',
                'key' => 'main'
            ],
            [
                'title' => 'О нас',
                'key' => 'about'
            ],
            [
                'title' => 'Услуги',
                'key' => 'services'
            ],
            [
                'title' => 'FAQ',
                'key' => 'faq'
            ],
            [
                'title' => 'Контакты',
                'key' => 'contacts'
            ],
            [
                'title' => 'Команда',
                'key' => 'team'
            ],
            [
                'title' => 'Вакансии',
                'key' => 'vacancies'
            ],
            [
                'title' => 'Пресса о нас',
                'key' => 'press_about_us'
            ],
            [
                'title' => 'Как стать исполнителем',
                'key' => 'how_to_become_a_executor'
            ],
            [
                'title' => 'Безопасность и гарантии',
                'key' => 'security and warranty'
            ],
            [
                'title' => 'Публичная офферта',
                'key' => 'offert'
            ],
            [
                'title' => 'Правила сайта',
                'key' => 'rules'
            ]
        ];

        foreach ($seosAr as $value) {
            $seo = $repository->findOneBy(['key' => $value['key']]);
            if (is_null($seo)) {
                $seo = new Seo;
                $seo
                    ->setTitle($value['title'])
                    ->setKey($value['key']);
                $em->persist($seo);
            }
        }
        $em->flush();

        $output->writeln('Finished');
    }
}