<?php

namespace AppBundle\Command;

use AppBundle\Entity\Option;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetBalanceCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:user:set-balance')
            ->addArgument('id', InputArgument::REQUIRED, 'The id of the User.')
            ->addArgument('balance', InputArgument::REQUIRED, 'The value of balance')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Begin');

        $id = $input->getArgument('id');
        $balance = $input->getArgument('balance');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $sto = $em->getRepository('AppBundle:User')->find($id);
        $sto->setBalance($balance);

        $em->flush();

        $output->writeln('Finished');
    }
}