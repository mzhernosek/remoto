<?php

namespace AppBundle\Command;

use AppBundle\Entity\Option;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetOrderCityCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:order:set-city')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Begin');


        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $orders = $em->getRepository('AppBundle:Order')->findAll();

        $city = $em->getRepository('AppBundle:City')->findOneBy(['title' => 'Витебск']);

        foreach ($orders as $order) {
            $order->setCity($city);

        }

        $em->flush();

        $output->writeln('Finished');
    }
}