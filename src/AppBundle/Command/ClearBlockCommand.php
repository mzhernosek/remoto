<?php

namespace AppBundle\Command;

use AppBundle\Entity\Option;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClearBlockCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:sto:clear-blocked')
            ->addArgument('id', InputArgument::REQUIRED, 'The id of the STO.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Begin');

        $id = $input->getArgument('id');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $sto = $em->getRepository('AppBundle:User')->find($id);
        $sto->setBlocked(0);

        $em->flush();

        $output->writeln('Finished');
    }
}