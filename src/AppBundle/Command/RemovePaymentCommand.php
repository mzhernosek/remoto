<?php

namespace AppBundle\Command;

use AppBundle\Entity\Option;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RemovePaymentCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:payment:remove')
            ->addArgument('id', InputArgument::REQUIRED, 'The id of the User.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Begin');

        $id = $input->getArgument('id');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $sto = $em->getRepository('AppBundle:Payment\Payment')->find($id);
        $em->remove($sto);

        $em->flush();

        $output->writeln('Finished');
    }
}