<?php

namespace AppBundle\Command;

use AppBundle\Entity\Option;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadOptionsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:load:options')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Begin');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository('AppBundle:Option');
        $optionsAr = [
            [
                'title' => 'Кол-во бесплатных дней для мастеров, салонов',
                'key' => 'free_days_count',
                'value' => 5
            ],
            [
                'title' => 'Кол-во баллов за день',
                'key' => 'points_for_day',
                'value' => 1
            ],
            [
                'title' => 'Кол-во баллов за каждые потраченные 30 рублей',
                'key' => 'points_for_pay',
                'value' => 1
            ],
            [
                'title' => '% к баллам за покупку PRO',
                'key' => 'percents_for_pro',
                'value' => 30
            ],
            [
                'title' => 'Баллов за заполнение профиля',
                'key' => 'points_for_full_profile',
                'value' => 100
            ],
            [
                'title' => 'Баллов за верификацию',
                'key' => 'points_for_verify',
                'value' => 20
            ],
            [
                'title' => 'Видео на главной',
                'key' => 'main_video',
                'value' => ''
            ],
            [
                'title' => 'Изображения в слайдере',
                'key' => 'main_images',
                'value' => ''
            ]
        ];

        foreach ($optionsAr as $value) {
            $option = $repository->findOneBy(['key' => $value['key']]);
            if (is_null($option)) {
                $option = new Option;
                $option
                    ->setTitle($value['title'])
                    ->setKey($value['key'])
                    ->setValue($value['value']);
                $em->persist($option);
            }
        }
        $em->flush();

        $output->writeln('Finished');
    }
}