<?php

namespace AppBundle\Service;

use AppBundle\Entity\Order;
use AppBundle\Entity\SmsConfirm;
use Symfony\Component\DependencyInjection\Container;

class SmsService
{
    /**
     * @var Container
     */
    private $container;

    public function __construct (Container $container)
    {
        $this->container = $container;

    }

    public function sendSmsConfirm ($phone)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $checkUser = $em->getRepository('AppBundle:User')->findOneBy(['phone' => $phone]);

        if ($checkUser) {
            return false;
        }

        $min = pow(10,4);
        $max = pow(10,5)-1;
        $code = rand($min, $max);

        $date = new \DateTime();
        $date->modify('+10 minutes');

        $smsConfirm = new SmsConfirm();
        $smsConfirm
            ->setPhone($phone)
            ->setCode($code)
            ->setDateCreated($date);

        $em->persist($smsConfirm);
        $em->flush();

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, 'http://api.rocketsms.by/json/send');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "username=391225716&password=2ufruHz5&priority=true&phone=" . $phone . "&text=Ваш код: " . $code);

        $result = @json_decode(curl_exec($curl), true);

        return $code;
    }

    public function sendNewOfferSms (Order $order)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, 'http://api.rocketsms.by/json/send');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "username=391225716&password=2ufruHz5&phone=" . $order->getOwner()->getPhone() . "&text=У Вашей заявки №". $order->getId() .' появилось новое предложение. http://remoto.by');

        $result = @json_decode(curl_exec($curl), true);
    }

    public function sendAnswerAcceptedSms(Order $order)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, 'http://api.rocketsms.by/json/send');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "username=391225716&password=2ufruHz5&phone=" . $order->getExecutor()->getPhone() . "&text=Ваше предложение к заявке №". $order->getId() .' принято.Номер клиента: +'.$order->getOwner()->getPhone());

        $result = @json_decode(curl_exec($curl), true);
    }

    public function checkSmsConfirm ($code)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = $em->createQuery('SELECT s FROM AppBundle:SmsConfirm s WHERE (s.code=:code) AND (s.date_created > CURRENT_DATE()) AND (s.date_confirmed is NULL)');
        $query->setParameter('code', $code, 'integer');
        $result = $query->getOneOrNullResult();

        if ($result) {
            $this->container->get('session')->set('ph_confirmed', $result->getPhone());

            $result->setDateConfirmed(new \DateTime());
            $em->persist($result);
            $em->flush();
        }

        return !is_null($result);
    }


}