<?php

namespace AppBundle\Twig;

use AppBundle\Entity\Order;
use AppBundle\Entity\User;
use AppBundle\Geoip\Geoip;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class FrontExtension extends \Twig_Extension
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var Geoip
     */
    private $geoip;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->request = $container->get('request_stack')->getCurrentRequest();
        $this->geoip = $container->get('app.geoip');
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getPage', array($this, 'getPage')),
            new \Twig_SimpleFunction('getOption', array($this, 'getOption')),
            new \Twig_SimpleFunction('getImages', array($this, 'getImages')),
            new \Twig_SimpleFunction('getWaitOrdersCount', array($this, 'getWaitOrdersCount')),
            new \Twig_SimpleFunction('getActiveOrdersCount', array($this, 'getActiveOrdersCount')),
            new \Twig_SimpleFunction('getCity', array($this, 'getCity')),
            new \Twig_SimpleFunction('getCities', array($this, 'getCities')),

        ];
    }

    public function getCities()
    {
        return $this->container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:City')->findAll();
    }

    public function getCity()
    {
        return $this->geoip->getCity();
    }


    public function getWaitOrdersCount()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        if (!$user) {
            return 0;
        }

        if ($user->getType() == User::PROFILE_TYPE_USER) {
            $query = $this->container->get('doctrine.orm.entity_manager')->createQuery('SELECT COUNT(o.id) FROM AppBundle:Order o WHERE o.status=:status AND o.owner=:user');
            $query->setParameter('status', Order::STATUS_NEW);
            $query->setParameter('user', $user);

            $count = $query->getSingleScalarResult();
        } else {
            $qb = $this->container->get('doctrine.orm.entity_manager')->createQueryBuilder();

            //todo: Оптимизировать
            $qb
                ->select('o')
                ->from('AppBundle:Order', 'o')
                ->where('o.status=:status')
                ->join('o.answers', 'a', 'WITH', 'a.owner = :sto')
                ->orderBy('o.date_created', 'desc')
                ->groupBy('o')
                ->setParameter('status', Order::STATUS_NEW)
                ->setParameter('sto', $user)
            ;

            $count = count($qb->getQuery()->getArrayResult());

        }


        return $count;
    }

    public function getActiveOrdersCount()
    {

        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        if (!$user) {
            return 0;
        }

        if ($user->getType() == User::PROFILE_TYPE_USER) {
            $query = $this->container->get('doctrine.orm.entity_manager')->createQuery('SELECT COUNT(o.id) FROM AppBundle:Order o WHERE o.status=:status AND o.owner=:user');
            $query->setParameter('status', Order::STATUS_IN_PROGRESS);
            $query->setParameter('user', $this->container->get('security.token_storage')->getToken()->getUser());

            $count = $query->getSingleScalarResult();
        } else {
            $qb = $this->container->get('doctrine.orm.entity_manager')->createQueryBuilder();

            //todo: Оптимизировать
            $qb
                ->select('o')
                ->from('AppBundle:Order', 'o')
                ->where('o.status=:status')
                ->join('o.answers', 'a', 'WITH', 'a.owner = :sto')
                ->orderBy('o.date_created', 'desc')
                ->groupBy('o')
                ->setParameter('status', Order::STATUS_IN_PROGRESS)
                ->setParameter('sto', $user)
            ;

            $count = count($qb->getQuery()->getArrayResult());
        }


        return $count;
    }

    public function getPage($key)
    {
        $page = $this->em->getRepository('AppBundle:Seo')->findOneBy(['key' => $key]);

        if (is_null($page)) {
            return [
                'title' => '',
                'keywords' => '',
                'description' => ''
            ];
        }

        return $page;
    }

    public function getOption($key)
    {
        return $this->em->getRepository('AppBundle:Option')->findOneBy(['key' => $key])->getValue();
    }

    public function getImages()
    {
        $option = $this->em->getRepository('AppBundle:Option')->findOneBy(['key' => 'main_images']);

        return explode(',', $option->getValue());
    }

    public function getName()
    {
        return 'front_extension';
    }
}