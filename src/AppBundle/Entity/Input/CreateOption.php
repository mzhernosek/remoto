<?php

namespace AppBundle\Entity\Input;

use JMS\Serializer\Annotation as Serial;

class CreateOption
{
    /**
     * @Serial\Type("string")
     */
    public $value;
}