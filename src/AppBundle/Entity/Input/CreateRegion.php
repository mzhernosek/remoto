<?php

namespace AppBundle\Entity\Input;

use JMS\Serializer\Annotation as Serial;

class CreateRegion
{
    /**
     * @Serial\Type("string")
     */
    public $title;
}