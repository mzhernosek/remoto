<?php

namespace AppBundle\Entity\Input;

use JMS\Serializer\Annotation as Serial;

class CreateSto
{
    /**
     * @Serial\Type("string")
     */
    public $title;

    /**
     * @Serial\Type("string")
     */
    public $email;

    /**
     * @Serial\Type("string")
     */
    public $phone;

    /**
     * @Serial\Type("integer")
     */
    public $region;

    /**
     * @Serial\Type("integer")
     */
    public $city;

    /**
     * @Serial\Type("string")
     */
    public $description;

    /**
     * @Serial\Type("string")
     */
    public $address;

    /**
     * @Serial\Type("float")
     */
    public $lat;

    /**
     * @Serial\Type("float")
     */
    public $lon;

    /**
     * @Serial\Type("array")
     */
    public $services;

    /**
     * @Serial\Type("string")
     */
    public $slug;

    /**
     * @Serial\Type("boolean")
     */
    public $published;
}