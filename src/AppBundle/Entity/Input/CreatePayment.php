<?php

namespace AppBundle\Entity\Input;

use JMS\Serializer\Annotation as Serial;

class CreatePayment
{
    /**
     * @Serial\Type("float")
     */
    public $total;

    /**
     * @Serial\Type("string")
     */
    public $text;
}