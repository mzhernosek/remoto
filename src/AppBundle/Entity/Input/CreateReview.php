<?php

namespace AppBundle\Entity\Input;

use JMS\Serializer\Annotation as Serial;

class CreateReview
{
    /**
     * @Serial\Type("string")
     */
    public $name;

    /**
     * @Serial\Type("string")
     */
    public $mark;

    /**
     * @Serial\Type("string")
     */
    public $text;
}