<?php

namespace AppBundle\Entity\Input;

use JMS\Serializer\Annotation as Serial;

class CreateCategory
{
    /**
     * @Serial\Type("string")
     */
    public $title;

    /**
     * @Serial\Type("integer")
     */
    public $parent;
}