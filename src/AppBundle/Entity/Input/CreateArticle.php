<?php

namespace AppBundle\Entity\Input;

use JMS\Serializer\Annotation as Serial;

class CreateArticle
{
    /**
     * @Serial\Type("string")
     */
    public $title;

    /**
     * @Serial\Type("boolean")
     */
    public $published;

    /**
     * @Serial\Type("string")
     */
    public $description;

    /**
     * @Serial\Type("string")
     */
    public $text;

    /**
     * @Serial\Type("string")
     */
    public $short_text;
}