<?php

namespace AppBundle\Entity\Input;

use JMS\Serializer\Annotation as Serial;

class CreateCar
{
    /**
     * @Serial\Type("string")
     */
    public $title;
}