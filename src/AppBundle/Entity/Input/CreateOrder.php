<?php

namespace AppBundle\Entity\Input;

use JMS\Serializer\Annotation as Serial;

class CreateOrder
{
    /**
     * @Serial\Type("string")
     */
    public $title;

    /**
     * @Serial\Type("string")
     */
    public $text;

    /**
     * @Serial\Type("integer")
     */
    public $type;
}