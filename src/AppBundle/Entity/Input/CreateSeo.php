<?php

namespace AppBundle\Entity\Input;

use JMS\Serializer\Annotation as Serial;

class CreateSeo
{
    /**
     * @Serial\Type("string")
     */
    public $title;

    /**
     * @Serial\Type("string")
     */
    public $keywords;

    /**
     * @Serial\Type("string")
     */
    public $description;

    /**
     * @Serial\Type("string")
     */
    public $text;
}