<?php
namespace AppBundle\Entity\Output;


use JMS\Serializer\Annotation as Serial;

/**
 * Class Rest
 * @package AppBundle\Entity\Output
 */
class Rest
{
    /**
     * @var int
     *
     * @Serial\Groups({"default"})
     */
    public $code;

    /**
     * @var string
     *
     * @Serial\Groups({"default"})
     */
    public $message;

    /**
     * @var array|null
     *
     * @Serial\Groups({"default"})
     */
    public $data;

    public function __construct($code, $message, $data = null)
    {
        $this->code = $code;
        $this->message = $message;
        if (!is_null($data)) {
            $this->data = $data;
        } else {
            unset($this->data);
        }
    }
}