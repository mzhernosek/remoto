<?php

namespace AppBundle\Geoip;

use Doctrine\ORM\Repository\RepositoryFactory;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\User;
use AppBundle\Geoip\SxGeo;

class Geoip
{
    private $country;
    private $region;
    private $city;

    /**
     * @var Container
     */
    private $container;

    private function getRegionRepository()
    {
        return $this->container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:Region');
    }

    /**
     * @return mixed
     */
    private function getCityRepository()
    {
        return $this->container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:City');
    }

    private function getCountryRepository()
    {
        return $this->container->get('doctrine.orm.entity_manager')->getRepository('AppBundle:Country');
    }

    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->setLocationByIp();
    }

    public function setLocationByUser(User $user)
    {
        if ($user->getCountry()) {
            $this->country = [
                'id' => $user->getCountry()->getId(),
                'title' => $user->getCountry()->getCountryNameRu()
            ];

            if ($user->getRegion()) {
                $this->region = [
                    'id' => $user->getRegion()->getId(),
                    'title' => $user->getRegion()->getRegionNameRu()
                ];

                if ($user->getCity()) {
                    $this->city = [
                        'id' => $user->getCity()->getId(),
                        'title' => $user->getCity()->getCityNameRu()
                    ];
                }
            }

        }



    }

    public function setLocationByIp()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request) {

            if ($this->container->get('security.token_storage')->getToken() && is_object($this->container->get('security.token_storage')->getToken()->getUser())) {
                $this->city = $this->container->get('security.token_storage')->getToken()->getUser()->getCity();
            } else {
                if (!$request->getSession()->has('cityByIp')) {
                    $env = $this->container->get('kernel')->getEnvironment();
                    if ($env == 'dev' || $env == 'test') {
                        $ip = '37.212.104.76'; // Витебск
                        $ip = '86.57.160.65'; // Гродно

//                    $ip = '62.16.64.0'; // россия
                    } else {
                        $ip = $request->getClientIp();
                    }

                    $sxGeo = new SxGeo('SxGeoCity.dat');

                    $city = $sxGeo->getCity($ip);

                    if (!empty($city)) {
                        $cityTitle = $city['city']['name_ru'];
                    } else {
                        $cityTitle = 'Витебск';
                    }

                    $cityObj = $this->getCityRepository()->findOneBy(['title' => $cityTitle]);

                    if (is_null($cityObj)) {
                        $cityObj = $this->getCityRepository()->findOneBy(['title' => 'Витебск']);
                    }

                    $this->city = $cityObj;
                    $request->getSession()->set('cityByIp', $this->city->getId());
                } else {
                    $this->city = $this->getCityRepository()->find($request->getSession()->get('cityByIp'));
                }
            }


        }
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function getDetectedLocation()
    {


        if ($this->city) {
            return $this->city['title'];
        }
        if ($this->region) {
            return $this->region['title'];
        }
        if ($this->country) {
            return $this->country['title'];
        }

        return false;
    }
}