<?php

namespace AppBundle\Security;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Order;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AnswerVoter extends Voter
{
    const DELETE = 'delete';

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::DELETE])) {
            return false;
        }

        if (!$subject instanceof Answer) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /** @var Answer $answer */
        $answer = $subject;

        switch ($attribute) {
            case self::DELETE:
                return $this->canDelete($answer, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canDelete(Answer $answer, User $user)
    {
        return $user === $answer->getOwner();
    }

}