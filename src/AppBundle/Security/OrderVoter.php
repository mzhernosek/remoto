<?php

namespace AppBundle\Security;

use AppBundle\Entity\Order;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class OrderVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::DELETE])) {
            return false;
        }

        if (!$subject instanceof Order) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /** @var Order $order */
        $order = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($order, $user);
            case self::EDIT:
                return $this->canEdit($order, $user);
            case self::DELETE:
                return $this->canDelete($order, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canDelete(Order $order, User $user)
    {
        return $user === $order->getOwner() && !$order->getExecutor();
    }

    private function canEdit(Order $order, User $user)
    {
        return $user === $order->getOwner() && $order->getStatus() === Order::STATUS_NEW;
    }

    private function canView(Order $order, User $user)
    {
        return $user === $order->getOwner() || $user->getType() === User::PROFILE_TYPE_STO;
    }
}