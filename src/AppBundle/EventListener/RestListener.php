<?php
namespace AppBundle\EventListener;


use AppBundle\Configuration as Application;
use AppBundle\Entity\Output;

use Doctrine\Common\Util\ClassUtils;
use Doctrine\Common\Annotations\Reader;
use FOS\RestBundle\Decoder\ContainerDecoderProvider;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

class RestListener
{
    /**
     * @var Reader
     */
    protected $reader;

    public function __construct(ContainerDecoderProvider $reader)
    {
        $this->reader = $reader;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        /** @var array $controller */
        $controller = $event->getController();
        if (!is_array($controller)) {
            return;
        }

        $className = class_exists('Doctrine\Common\Util\ClassUtils') ? ClassUtils::getClass($controller[0]) : get_class($controller[0]);
        $object = new \ReflectionClass($className);
        $method = $object->getMethod($controller[1]);

        $restAnnotation = false;
        $methodAnnotations = $this->reader->getMethodAnnotations($method);
        foreach ($methodAnnotations as $annotation) {
            if ($annotation instanceof Application\Rest) {
                $restAnnotation = $annotation;
            }
        }
        if (!$restAnnotation) {
            $classAnnotations = $this->reader->getClassAnnotations($object);
            foreach ($classAnnotations as $annotation) {
                if ($annotation instanceof Application\Rest) {
                    $restAnnotation = $annotation;
                }
            }
        }
        if (!$restAnnotation) {
            return;
        }

        $event->getRequest()->attributes->set('_rest_annotation', $restAnnotation);
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        /** @var Application\Rest|null $restAnnotation */
        $restAnnotation = $event->getRequest()->attributes->get('_rest_annotation');
        if (!$restAnnotation) {
            return;
        }
        $result = $event->getControllerResult();
        if ($result instanceof Output\Rest) {
            return;
        }
        if ($result instanceof View) {
            return;
        }
        if ($result instanceof Response) {
            return;
        }

        $result = new Output\Rest(
            $restAnnotation->getCode(),
            $restAnnotation->getMessage(),
            $event->getControllerResult()
        );
        $event->setControllerResult($result);
    }
}