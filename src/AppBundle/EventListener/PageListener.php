<?php
namespace AppBundle\EventListener;


use AppBundle\Configuration as Application;
use AppBundle\Entity\Output;

use Doctrine\Common\Util\ClassUtils;
use Doctrine\Common\Annotations\Reader;
use Sensio\Bundle\FrameworkExtraBundle\Templating\TemplateGuesser;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

class PageListener
{
    /**
     * @var Reader
     */
    protected $reader;

    /**
     * @var TemplateGuesser
     */
    protected $guesser;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    public function __construct(Reader $reader, TemplateGuesser $guesser, \Twig_Environment $twig)
    {
        $this->reader = $reader;
        $this->guesser = $guesser;
        $this->twig = $twig;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        /** @var array $controller */
        $controller = $event->getController();
        if (!is_array($controller)) {
            return;
        }

        $className = class_exists('Doctrine\Common\Util\ClassUtils') ? ClassUtils::getClass($controller[0]) : get_class($controller[0]);
        $object = new \ReflectionClass($className);
        $method = $object->getMethod($controller[1]);

        $pageAnnotation = false;
        $methodAnnotations = $this->reader->getMethodAnnotations($method);
        foreach ($methodAnnotations as $annotation) {
            if ($annotation instanceof Application\Page) {
                $pageAnnotation = $annotation;
            }
        }
        if (!$pageAnnotation) {
            $classAnnotations = $this->reader->getClassAnnotations($object);
            foreach ($classAnnotations as $annotation) {
                if ($annotation instanceof Application\Page) {
                    $pageAnnotation = $annotation;
                }
            }
        }
        if (!$pageAnnotation) {
            return;
        }

        if (!$pageAnnotation->getTemplate()) {
            $pageAnnotation->setTemplate($this->guesser->guessTemplateName($controller, $event->getRequest(), 'twig'));
        }

        $event->getRequest()->attributes->set('_page_annotation', $pageAnnotation);
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        /** @var Application\Page|null $pageAnnotation */
        $pageAnnotation = $event->getRequest()->attributes->get('_page_annotation');
        if (!$pageAnnotation) {
            return;
        }
        $result = $event->getControllerResult();
        if ($result instanceof Response) {
            return;
        }


        /** @var \Twig_Template $template */
        $template = $this->twig->loadTemplate($pageAnnotation->getTemplate());
        $breadcrumbs = $template->renderBlock('breadcrumbs', []);
        $result = new Output\Page(
            $pageAnnotation->getTitle(),
            $breadcrumbs,
            $event->getControllerResult()
        );
        $event->setControllerResult($result);
        $event->setResponse(new JsonResponse($result));
    }
}