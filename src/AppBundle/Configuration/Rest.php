<?php
namespace AppBundle\Configuration;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationAnnotation;

/**
 * Class Rest
 * @package AppBundle\Configuration
 *
 * @Annotation
 */
class Rest extends ConfigurationAnnotation
{
    protected $code = 0;
    protected $message = 'Ok';

    public function setValue($message)
    {
        $this->setMessage($message);
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function allowArray()
    {
        return false;
    }

    public function getAliasName()
    {
        return 'Rest';
    }
}