<?php

namespace AppBundle\Manager;

use \FOS\UserBundle\Doctrine\UserManager as BaseManager;

Class UserManager extends BaseManager
{
    public function findUserByUsernameOrEmail($usernameOrEmail)
    {
        if (preg_match('/^.+\@\S+\.\S+$/', $usernameOrEmail)) {
            return $this->findUserByEmail($usernameOrEmail);
        }

        return $this->findUserBy(['phone' => $usernameOrEmail]);
    }
}