<?php

namespace AppBundle\Mailer;

use AppBundle\Entity\Order;
use FOS\UserBundle\Mailer\TwigSwiftMailer as BaseMailer;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

Class Mailer extends BaseMailer
{
    public function sendStoCreatedMessage(UserInterface $user, $password, $orders)
    {
        $template = 'email/sto_confirm.twig';

        $this->sendMessage($template,['user' => $user, 'password' => $password, 'orders' => $orders], $this->parameters['from_email']['confirmation'], (string) $user->getEmail());
    }

    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        $template = 'email/user_confirm.twig';
        $url = $this->router->generate('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);

        $context = array(
            'user' => $user,
            'confirmationUrl' => $url,
        );

        $this->sendMessage($template, $context, $this->parameters['from_email']['confirmation'], (string) $user->getEmail());
    }

    public function sendNewOfferEmail(Order $order)
    {
        $template = 'email/new_offer.twig';

        $context = array(
            'order' => $order
        );

        $this->sendMessage($template, $context, $this->parameters['from_email']['confirmation'], (string) $order->getOwner()->getEmail());
    }

    public function sendAnswerAcceptedEmail(Order $order)
    {
        $template = 'email/accepted_offer.twig';

        $context = array(
            'order' => $order
        );

        $this->sendMessage($template, $context, $this->parameters['from_email']['confirmation'], (string) $order->getExecutor()->getEmail());
    }

    public function sendResettingEmailMessage(UserInterface $user)
    {
        $template = 'email/password_resetting.twig';
        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);

        $context = array(
            'user' => $user,
            'confirmationUrl' => $url,
        );

        $this->sendMessage($template, $context, $this->parameters['from_email']['resetting'], (string) $user->getEmail());
    }


}