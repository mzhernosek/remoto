#!/bin/bash

git stash
git pull
php bin/console doctrine:schema:update --force
php bin/console doctrine:mongodb:schema:update
php bin/console cache:clear --env=prod
php bin/console cache:clear --env=dev
php bin/console assets:install
chmod 777 bin/cache/ -R
chmod 777 bin/logs/ -R
chmod 777 ./build.sh
chmod 777 ./test.sh
